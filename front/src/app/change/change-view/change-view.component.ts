import { Component, OnInit } from '@angular/core';
import {Change} from '../../model/change.model';
import {ChangeDataService} from '../../service/change-data.service';
import {ActivatedRoute} from '@angular/router';

@Component({
  selector: 'app-change-view',
  templateUrl: './change-view.component.html',
  styleUrls: ['./change-view.component.css']
})
export class ChangeViewComponent implements OnInit {

  change: Change;
  id: number;




  constructor(private changeService: ChangeDataService, private route: ActivatedRoute) { }

  ngOnInit() {
    this.id = this.route.snapshot.params.id;

    this.change = new Change(this.id,
      '',
      new Date(),
      '',
      '',
      '',
      '',
      '',
      '',
      '',
      new Date(),
      new Date(),
      '',
      '',
      '',
      '',
      '',
      '',
      new Date(),
      new Date(),
      new Date(),
      new Date(),
      new Date()
    );

    this.changeService.getChange(this.id).subscribe(
      data => {
        this.change = data;
        console.log(this.change);
      }
    );
  }

}
