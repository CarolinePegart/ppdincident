import { Component, OnInit } from '@angular/core';
import {Subscription} from 'rxjs';
import {Router} from '@angular/router';
import {ChangeDataService} from '../../service/change-data.service';

@Component({
  selector: 'app-change-list',
  templateUrl: './change-list.component.html',
  styleUrls: ['./change-list.component.css']
})
export class ChangeListComponent implements OnInit {

  changeList = [];
  lastIdChangeList: any;


  private changesSubscription: Subscription;

  constructor(private changeService: ChangeDataService, private router: Router) { }

  ngOnInit() {
    this.changesSubscription = this.changeService.changesSubject.subscribe(
      (changes: any) => {
        this.changeList = changes['hydra:member'];

        this.lastIdChangeList = this.changeList[this.changeList.length - 1].id;
        console.log(this.lastIdChangeList);

      }
    );
    this.changeService.getChangeList();

    this.changeService.currentChangeNumber.subscribe(lastIdChangeList =>
      this.lastIdChangeList = lastIdChangeList);
  }

  createChange() {
    // Envoie le dernier id de table incident au component add-incident
    this.changeService.majChangeNumber(this.lastIdChangeList);
    // Renvoie sur la page du formulaire pour créer un incident
    this.router.navigate(['change/add']);

  }

}
