import { Component, OnInit } from '@angular/core';
import {Subscription} from 'rxjs';
import {Router} from '@angular/router';
import {Change} from '../../model/change.model';
import {ChangeDataService} from '../../service/change-data.service';


@Component({
  selector: 'app-change-add',
  templateUrl: './change-add.component.html',
  styleUrls: ['./change-add.component.css']
})
export class ChangeAddComponent implements OnInit {

  change: Change;
  id: number;
  idChange: any;


  constructor(private changeService: ChangeDataService, private router: Router) { }

  ngOnInit() {
    this.changeService.currentChangeNumber.subscribe(idChange =>
      this.idChange = idChange);


    this.change = new Change(this.id,
      `CHG`.concat(this.idChange + 1),
      new Date(),
      '',
      '',
      '',
      '',
      '',
      '',
      '',
      new Date(),
      new Date(),
      '',
      '',
      '',
      '',
      '',
      '',
      new Date(),
      new Date(),
      new Date(),
      new Date(),
      new Date()
      );

  }

  createChange() {
    this.changeService.postChange(this.change).subscribe(
      data => {
        console.log(data);
        this.router.navigate(['change/list']);
      }
    );
  }

}
