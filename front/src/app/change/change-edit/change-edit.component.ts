import { Component, OnInit } from '@angular/core';
import {ActivatedRoute, Router} from '@angular/router';
import {Change} from '../../model/change.model';
import {ChangeDataService} from '../../service/change-data.service';

@Component({
  selector: 'app-change-edit',
  templateUrl: './change-edit.component.html',
  styleUrls: ['./change-edit.component.css']
})
export class ChangeEditComponent implements OnInit {

  change: Change;
  id: number;


  constructor(private changeService: ChangeDataService, private route: ActivatedRoute, private router: Router) { }

  ngOnInit() {
    this.id = this.route.snapshot.params.id;


    this.change = new Change(this.id,
      '',
      new Date(),
      '',
      '',
      '',
      '',
      '',
      '',
      '',
      new Date(),
      new Date(),
      '',
      '',
      '',
      '',
      '',
      '',
      new Date(),
      new Date(),
      new Date(),
      new Date(),
      new Date()
    );

    this.changeService.getChange(this.id).subscribe(
      data => {
        this.change = data;
        console.log(data);
      }
    );
  }

  editChange() {
    this.changeService.updateChange(this.id, this.change).subscribe(
      data => {
        console.log(data);
        this.router.navigate(['change/list']);
      }
    );
  }

}
