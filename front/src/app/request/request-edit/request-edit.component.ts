import { Component, OnInit } from '@angular/core';
import {Subscription} from 'rxjs';
import {RequestDataService} from '../../service/request-data.service';
import {CustomerDataService} from '../../service/customer-data.service';
import {ActivatedRoute, Router} from '@angular/router';
import {Request} from '../../model/request.model';

@Component({
  selector: 'app-request-edit',
  templateUrl: './request-edit.component.html',
  styleUrls: ['./request-edit.component.css']
})
export class RequestEditComponent implements OnInit {

  id: number;
  request: Request;

  customerList = [];
  private customersSubscription: Subscription;

  // tslint:disable-next-line:max-line-length
  constructor(private requestService: RequestDataService, private customerService: CustomerDataService, private route: ActivatedRoute, private router: Router) { }

  ngOnInit() {
    this.id = this.route.snapshot.params.id;

    this.request = new Request(
      this.id,
      '',
      new Date(),
      '',
      '',
      '',
      '',
      '',
      '',
      '',
      '',
      '',
      '',
      '',
      '',
      '',
      new Date(),
      new Date()
    );

    this.requestService.getRequest(this.id).subscribe(
      data => {
        this.request = data;
        console.log(data);
      },
    );

    console.log(this.request);

    this.customersSubscription = this.customerService.customersSubject.subscribe(
      (customers: any) => {
        this.customerList = customers['hydra:member'];
        console.log(this.customerList);

      }
    );
    this.customerService.getCustomerList();

  }

  editRequest() {
    this.requestService.updateRequest(this.id, this.request).subscribe(
      data => {
        console.log(data);
        this.router.navigate(['request/list']);

      }
    );
  }

}
