import { Component, OnInit } from '@angular/core';
import { Request } from '../../model/request.model';
import {Subscription} from 'rxjs';
import {RequestDataService} from '../../service/request-data.service';
import {ActivatedRoute} from '@angular/router';
import {CustomerDataService} from '../../service/customer-data.service';

@Component({
  selector: 'app-request-view',
  templateUrl: './request-view.component.html',
  styleUrls: ['./request-view.component.css']
})
export class RequestViewComponent implements OnInit {
  id: number;
  request: Request;
  customerList = [];
  private customersSubscription: Subscription;

  constructor(private requestService: RequestDataService, private route: ActivatedRoute, private customerService: CustomerDataService) { }

  ngOnInit() {
    this.id = this.route.snapshot.params.id;

    this.request = new Request(
      this.id,
      '',
      new Date(),
      '',
      '',
      '',
      '',
      '',
      '',
      '',
      '',
      '',
      '',
      '',
      '',
      '',
      new Date(),
      new Date()
    );

    this.requestService.getRequest(this.id).subscribe(
      data => {
        this.request = data;
        console.log(this.request);
      }
    );

    this.customersSubscription = this.customerService.customersSubject.subscribe(
      (customers: any) => {
        this.customerList = customers['hydra:member'];
        console.log(this.customerList);

      }
    );
    this.customerService.getCustomerList();
  }

}
