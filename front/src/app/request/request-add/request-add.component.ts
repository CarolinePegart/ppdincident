import { Component, OnInit } from '@angular/core';
import {Subscription} from 'rxjs';
import {RequestDataService} from '../../service/request-data.service';
import { Request } from '../../model/request.model';
import {Router} from '@angular/router';
import {CustomerDataService} from '../../service/customer-data.service';

@Component({
  selector: 'app-request-add',
  templateUrl: './request-add.component.html',
  styleUrls: ['./request-add.component.css']
})
export class RequestAddComponent implements OnInit {

  request: Request;
  id: number;
  idRequest: any;

  customerList = [];
  private customersSubscription: Subscription;

  constructor(private requestService: RequestDataService, private router: Router, private customerService: CustomerDataService) { }

  ngOnInit() {
    this.requestService.currentRequestNumber.subscribe(
      idRequest => this.idRequest = idRequest);

    this.request = new Request(
      this.id,
      `REQ`.concat(this.idRequest + 1),
      new Date(),
      '',
      '',
      '',
      '',
      '',
      '',
      '',
      '',
      '',
      '',
      '',
      '',
      '',
      new Date(),
      new Date()
    );
    this.customersSubscription = this.customerService.customersSubject.subscribe(
      (customers: any) => {
        this.customerList = customers['hydra:member'];
        console.log(this.customerList);

      }
    );
    this.customerService.getCustomerList();
  }

  createRequest() {
    this.requestService.postRequest(this.request).subscribe(
      data => {
        console.log(data);
        this.router.navigate(['request/list']);
      }
    );
  }

}
