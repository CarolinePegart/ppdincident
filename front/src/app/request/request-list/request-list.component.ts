import { Component, OnInit } from '@angular/core';
import {Subscription} from 'rxjs';
import {Router} from '@angular/router';
import {RequestDataService} from '../../service/request-data.service';

@Component({
  selector: 'app-request-list',
  templateUrl: './request-list.component.html',
  styleUrls: ['./request-list.component.css']
})
export class RequestListComponent implements OnInit {
  requestList = [];
  lastIdRequestList: any;


  private requestsSubscription: Subscription;

  constructor(private requestService: RequestDataService, private router: Router) { }

  ngOnInit() {
    this.requestsSubscription = this.requestService.requestsSubject.subscribe(
      (requests: any) => {
        this.requestList = requests['hydra:member'];

        this.lastIdRequestList = this.requestList[this.requestList.length - 1].id;
        console.log(this.lastIdRequestList);

      }
    );
    this.requestService.getRequestList();

    this.requestService.currentRequestNumber.subscribe(lastIdRequestList =>
      this.lastIdRequestList = lastIdRequestList);
  }

  createRequest() {
    // Envoie le dernier id de table incident au component add-incident
    this.requestService.majRequestNumber(this.lastIdRequestList);
    // Renvoie sur la page du formulaire pour créer un incident
    this.router.navigate(['request/add']);

  }

}
