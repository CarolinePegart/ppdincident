import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { IncidentListComponent } from './incident/incident-list/incident-list.component';
import { IncidentAddComponent } from './incident/incident-add/incident-add.component';
import { IncidentEditComponent } from './incident/incident-edit/incident-edit.component';
import { IncidentViewComponent } from './incident/incident-view/incident-view.component';
import {CustomerListComponent} from './customer/customer-list/customer-list.component';
import {CustomerAddComponent} from './customer/customer-add/customer-add.component';
import {CustomerEditComponent} from './customer/customer-edit/customer-edit.component';
import {ChangeListComponent} from './change/change-list/change-list.component';
import {ChangeAddComponent} from './change/change-add/change-add.component';
import {ChangeViewComponent} from './change/change-view/change-view.component';
import {ChangeEditComponent} from './change/change-edit/change-edit.component';
import {RequestListComponent} from './request/request-list/request-list.component';
import {RequestAddComponent} from './request/request-add/request-add.component';
import {RequestViewComponent} from './request/request-view/request-view.component';
import {RequestEditComponent} from './request/request-edit/request-edit.component';
import {ProblemListComponent} from './problem/problem-list/problem-list.component';
import {ProblemAddComponent} from './problem/problem-add/problem-add.component';
import {ProblemViewComponent} from './problem/problem-view/problem-view.component';
import {ProblemEditComponent} from './problem/problem-edit/problem-edit.component';
import {LoginComponent} from './login/login.component';
import {AuthenticationGuardService} from './service/authentication-guard.service';
import {UserListComponent} from './user/user-list/user-list.component';
import {UserAddComponent} from './user/user-add/user-add.component';
import {UserEditComponent} from './user/user-edit/user-edit.component';
import {DashboardComponent} from './dashboard/dashboard.component';


const routes: Routes = [
  {path: 'incident/list', component: IncidentListComponent, canActivate: [AuthenticationGuardService]},
  {path: 'incident/add', component: IncidentAddComponent, canActivate: [AuthenticationGuardService]},
  {path: 'incident/edit/:id', component: IncidentEditComponent, canActivate: [AuthenticationGuardService]},
  {path: 'incident/view/:id', component: IncidentViewComponent, canActivate: [AuthenticationGuardService]},
  {path: 'customer/list', component: CustomerListComponent, canActivate: [AuthenticationGuardService]},
  {path: 'customer/add', component: CustomerAddComponent, canActivate: [AuthenticationGuardService]},
  {path: 'customer/edit/:id', component: CustomerEditComponent, canActivate: [AuthenticationGuardService]},
  {path: 'change/list', component: ChangeListComponent, canActivate: [AuthenticationGuardService]},
  {path: 'change/add', component: ChangeAddComponent, canActivate: [AuthenticationGuardService]},
  {path: 'change/view/:id', component: ChangeViewComponent, canActivate: [AuthenticationGuardService]},
  {path: 'change/edit/:id', component: ChangeEditComponent, canActivate: [AuthenticationGuardService]},
  {path: 'request/list', component: RequestListComponent, canActivate: [AuthenticationGuardService]},
  {path: 'request/add', component: RequestAddComponent, canActivate: [AuthenticationGuardService]},
  {path: 'request/view/:id', component: RequestViewComponent, canActivate: [AuthenticationGuardService]},
  {path: 'request/edit/:id', component: RequestEditComponent, canActivate: [AuthenticationGuardService]},
  {path: 'problem/list', component: ProblemListComponent, canActivate: [AuthenticationGuardService]},
  {path: 'problem/add', component: ProblemAddComponent, canActivate: [AuthenticationGuardService]},
  {path: 'problem/view/:id', component: ProblemViewComponent, canActivate: [AuthenticationGuardService]},
  {path: 'problem/edit/:id', component: ProblemEditComponent, canActivate: [AuthenticationGuardService]},
  {path: 'user/list', component: UserListComponent, canActivate: [AuthenticationGuardService]},
  {path: 'user/add', component: UserAddComponent, canActivate: [AuthenticationGuardService]},
  {path: 'user/edit/:id', component: UserEditComponent, canActivate: [AuthenticationGuardService]},
  {path: 'dashboard', component: DashboardComponent, canActivate: [AuthenticationGuardService]},
  {path: 'login', component: LoginComponent},
  { path: '', redirectTo: 'login', pathMatch: 'full' },

];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
