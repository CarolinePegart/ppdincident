import { Component, OnInit } from '@angular/core';
import {Router} from '@angular/router';
import {AuthenticationService} from '../../service/authentication.service';
import {Observable} from 'rxjs';
import {User} from '../../model/user.model';

@Component({
  selector: 'app-navbar',
  templateUrl: './navbar.component.html',
  styleUrls: ['./navbar.component.css']
})
export class NavbarComponent implements OnInit {
  isLoggedIn: Observable<User>;

  constructor(private router: Router, private authenticationService: AuthenticationService) { }

  ngOnInit() {
    this.isLoggedIn = this.authenticationService.currentUser;

  }

  logout() {
    // remove user from local storage to log user out
    this.authenticationService.logout();
    this.router.navigate(['login']);
  }

}
