import { Component, OnInit } from '@angular/core';
import {Problem} from '../../model/problem.model';
import {ProblemDataService} from '../../service/problem-data.service';
import {ActivatedRoute, Router} from '@angular/router';

@Component({
  selector: 'app-problem-edit',
  templateUrl: './problem-edit.component.html',
  styleUrls: ['./problem-edit.component.css']
})
export class ProblemEditComponent implements OnInit {

  id: number;
  problem: Problem;

  constructor(private problemService: ProblemDataService, private route: ActivatedRoute, private router: Router) { }

  ngOnInit() {
    this.id = this.route.snapshot.params.id;

    this.problem = new Problem(
      this.id,
      '',
      new Date(),
      '',
      '',
      '',
      '',
      '',
      '',
      '',
      new Date()
    );

    this.problemService.getProblem(this.id).subscribe(
      data => {
        this.problem = data;
        console.log(data);
      }
    );
  }

  editProblem() {
    this.problemService.updateProblem(this.id, this.problem).subscribe(
      data => {
        console.log(data);
        this.router.navigate(['problem/list']);

      }
    );
  }

}
