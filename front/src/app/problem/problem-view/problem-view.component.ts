import { Component, OnInit } from '@angular/core';
import {Problem} from '../../model/problem.model';
import {ProblemDataService} from '../../service/problem-data.service';
import {ActivatedRoute} from '@angular/router';

@Component({
  selector: 'app-problem-view',
  templateUrl: './problem-view.component.html',
  styleUrls: ['./problem-view.component.css']
})
export class ProblemViewComponent implements OnInit {

  id: number;
  problem: Problem;


  constructor(private problemService: ProblemDataService, private route: ActivatedRoute) { }

  ngOnInit() {
    this.id = this.route.snapshot.params.id;

    this.problem = new Problem(
      this.id,
      '',
       new Date(),
      '',
      '',
      '',
      '',
      '',
      '',
      '',
      new Date()
    );

    this.problemService.getProblem(this.id).subscribe(
      data => {
        this.problem = data;
        console.log(this.problem);
      }
    );
  }

}
