import { Component, OnInit } from '@angular/core';
import {Subscription} from 'rxjs';
import {ProblemDataService} from '../../service/problem-data.service';
import {Router} from '@angular/router';


@Component({
  selector: 'app-problem-list',
  templateUrl: './problem-list.component.html',
  styleUrls: ['./problem-list.component.css']
})
export class ProblemListComponent implements OnInit {

  problemList = [];
  lastIdProblemList: any;

  private problemsSubscription: Subscription;

  constructor(private problemService: ProblemDataService, private router: Router) { }

  ngOnInit() {
    this.problemsSubscription = this.problemService.problemsSubject.subscribe(
      (problems: any) => {
        this.problemList = problems['hydra:member'];

        this.lastIdProblemList = this.problemList[this.problemList.length - 1].id;
        console.log(this.lastIdProblemList);

      }
    );
    this.problemService.getProblemList();

    this.problemService.currentProblemNumber.subscribe(
      lastIdProblemList => this.lastIdProblemList = lastIdProblemList
    );
  }

  createProblem() {
    // Envoie le dernier id de table problem au component add-problem
    this.problemService.majProblemNumber(this.lastIdProblemList);
    // Renvoie sur la page du formulaire pour créer un incident
    this.router.navigate(['problem/add']);
  }

}
