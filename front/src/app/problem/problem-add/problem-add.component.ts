import { Component, OnInit } from '@angular/core';
import {Problem} from '../../model/problem.model';
import {ProblemDataService} from '../../service/problem-data.service';
import {Router} from '@angular/router';


@Component({
  selector: 'app-problem-add',
  templateUrl: './problem-add.component.html',
  styleUrls: ['./problem-add.component.css']
})
export class ProblemAddComponent implements OnInit {

  problem: Problem;
  id: number;
  idProblem: any;




  constructor(private problemService: ProblemDataService, private router: Router) {

  }


  ngOnInit() {
    this.problemService.currentProblemNumber.subscribe(
      idProblem => this.idProblem = idProblem);

    this.problem = new Problem(
      this.id,
      `PRB`.concat(this.idProblem + 1),
      new Date(),
      '',
      '',
      '',
      '',
      '',
      '',
       '',
       new Date()
      );


  }

  createProblem() {
    this.problemService.postProblem(this.problem).subscribe(
      data => {
        console.log(data);
        this.router.navigate(['problem/list']);
      }
    );
  }

}
