import { Injectable } from '@angular/core';
import {BehaviorSubject, Subject} from 'rxjs';
import {HttpClient} from '@angular/common/http';
import { Request } from '../model/request.model';

@Injectable({
  providedIn: 'root'
})
export class RequestDataService {
  private URL_BACKEND = 'http://127.0.0.1:8000/api';

  requestsSubject = new Subject<any>();
  requestList = [];

  private requestNumber = new BehaviorSubject('');
  currentRequestNumber = this.requestNumber.asObservable();


  constructor(private http: HttpClient) {
  }

  emitRequestsSubject() {
    this.requestsSubject.next(this.requestList);
  }

  majRequestNumber(message: string) {
    this.requestNumber.next(message);
  }

  getRequestList() {
    this.http.get(`${this.URL_BACKEND}/requests`).subscribe((response: any) => {
      this.requestList = response;
      console.log(response);
      this.emitRequestsSubject();
    });
  }

  // create request (back)
  postRequest(request) {
    return this.http.post(`${this.URL_BACKEND}/requests`, request);
  }

  // get request (back)
  getRequest(id) {
    return this.http.get<Request>(`${this.URL_BACKEND}/requests/${id}`);
  }

  // update request (back)
  updateRequest(id, request) {
    return this.http.put(`${this.URL_BACKEND}/requests/${id}`, request);
  }

}
