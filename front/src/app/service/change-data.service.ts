import { Injectable } from '@angular/core';
import {BehaviorSubject, Subject} from 'rxjs';
import {HttpClient} from '@angular/common/http';
import {Change} from '../model/change.model';

@Injectable({
  providedIn: 'root'
})
export class ChangeDataService {

  private URL_BACKEND = 'http://127.0.0.1:8000/api';

  changesSubject = new Subject<any>();
  changeList = [];

  private changeNumber = new BehaviorSubject('');
  currentChangeNumber = this.changeNumber.asObservable();


  constructor(private http: HttpClient) {
  }

  emitChangesSubject() {
    this.changesSubject.next(this.changeList);
  }

  majChangeNumber(message: string) {
    this.changeNumber.next(message);
  }

  getChangeList() {
    this.http.get(`${this.URL_BACKEND}/changes`).subscribe((response: any) => {
      this.changeList = response;
      console.log(response);
      this.emitChangesSubject();
    });
  }

  // create change (back)
  postChange(change) {
    return this.http.post(`${this.URL_BACKEND}/changes`, change);
  }

  // get change (back)
  getChange(id) {
    return this.http.get<Change>(`${this.URL_BACKEND}/changes/${id}`);
  }

  // update change (back)
  updateChange(id, change) {
    return this.http.put(`${this.URL_BACKEND}/changes/${id}`, change);
  }
}
