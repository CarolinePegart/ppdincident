import { Injectable } from '@angular/core';
import {Subject} from 'rxjs';
import {HttpClient} from '@angular/common/http';
import {User} from '../model/user.model';

@Injectable({
  providedIn: 'root'
})
export class UserDataService {

  private URL_BACKEND = 'http://127.0.0.1:8000/api';

  usersSubject = new Subject<any>();

  userList = [];



  constructor(private http: HttpClient) { }

  emitUsersSubject() {
    this.usersSubject.next(this.userList);
  }

  getUserList() {
    this.http.get(`${this.URL_BACKEND}/users`).subscribe((response: any) => {
      this.userList = response;
      console.log(response);
      this.emitUsersSubject();
    });
  }

  // create user (back)
  postUser(user) {
    return this.http.post(`${this.URL_BACKEND}/users`, user);
  }

  // get user (back)
  getUser(id) {
    return this.http.get<User>(`${this.URL_BACKEND}/users/${id}`);
  }

  // update the user (back)
  updateUser(id, user) {
    return this.http.put(`${this.URL_BACKEND}/users/${id}`, user);
  }

  // Remove the user
  deleteUser(id) {
    return this.http.delete(`${this.URL_BACKEND}/users/${id}`);
  }
}
