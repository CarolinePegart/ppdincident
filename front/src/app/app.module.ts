import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { NavbarComponent } from './navigation/navbar/navbar.component';
import { SidebarComponent } from './navigation/sidebar/sidebar.component';

import {FormsModule, ReactiveFormsModule} from '@angular/forms';
import {HTTP_INTERCEPTORS, HttpClientModule} from '@angular/common/http';
import { IncidentListComponent } from './incident/incident-list/incident-list.component';
import { IncidentAddComponent } from './incident/incident-add/incident-add.component';
import { IncidentEditComponent } from './incident/incident-edit/incident-edit.component';
import { IncidentViewComponent } from './incident/incident-view/incident-view.component';
import { CustomerListComponent } from './customer/customer-list/customer-list.component';
import { CustomerAddComponent } from './customer/customer-add/customer-add.component';
import { CustomerEditComponent } from './customer/customer-edit/customer-edit.component';
import { ChangeListComponent } from './change/change-list/change-list.component';
import { ChangeAddComponent } from './change/change-add/change-add.component';
import { ChangeViewComponent } from './change/change-view/change-view.component';
import { ChangeEditComponent } from './change/change-edit/change-edit.component';


import { ProblemAddComponent } from './problem/problem-add/problem-add.component';

import { RequestListComponent } from './request/request-list/request-list.component';
import { RequestAddComponent } from './request/request-add/request-add.component';
import { RequestViewComponent } from './request/request-view/request-view.component';
import { RequestEditComponent } from './request/request-edit/request-edit.component';
import { ProblemListComponent } from './problem/problem-list/problem-list.component';
import { ProblemViewComponent } from './problem/problem-view/problem-view.component';
import { ProblemEditComponent } from './problem/problem-edit/problem-edit.component';
import { LoginComponent } from './login/login.component';
import {JwtInterceptor} from './interceptor/jwt.interceptor';
import { UserListComponent } from './user/user-list/user-list.component';
import { UserAddComponent } from './user/user-add/user-add.component';
import { UserEditComponent } from './user/user-edit/user-edit.component';
import { DashboardComponent } from './dashboard/dashboard.component';


@NgModule({
  declarations: [
    AppComponent,
    NavbarComponent,
    SidebarComponent,
    IncidentListComponent,
    IncidentAddComponent,
    IncidentEditComponent,
    IncidentViewComponent,
    CustomerListComponent,
    CustomerAddComponent,
    CustomerEditComponent,
    ChangeListComponent,
    ChangeAddComponent,
    ChangeViewComponent,
    ChangeEditComponent,
    ProblemAddComponent,
    RequestListComponent,
    RequestAddComponent,
    RequestViewComponent,
    RequestEditComponent,
    ProblemListComponent,
    ProblemViewComponent,
    ProblemEditComponent,
    LoginComponent,
    UserListComponent,
    UserAddComponent,
    UserEditComponent,
    DashboardComponent

  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    HttpClientModule,
    FormsModule,
    ReactiveFormsModule
  ],
  providers: [
    { provide: HTTP_INTERCEPTORS, useClass: JwtInterceptor, multi: true },
  ],
  bootstrap: [AppComponent, NavbarComponent, SidebarComponent]
})
export class AppModule { }
