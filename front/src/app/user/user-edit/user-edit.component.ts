import { Component, OnInit } from '@angular/core';
import {User} from '../../model/user.model';
import {UserDataService} from '../../service/user-data.service';
import {ActivatedRoute, Router} from '@angular/router';

@Component({
  selector: 'app-user-edit',
  templateUrl: './user-edit.component.html',
  styleUrls: ['./user-edit.component.css']
})
export class UserEditComponent implements OnInit {
  user: User;
  id: number;

  constructor(private userService: UserDataService, private router: Router, private route: ActivatedRoute) { }

  ngOnInit() {
    this.id = this.route.snapshot.params.id;
    this.user = new User(
      this.id,
      '',
      '',
      '',
      '',
      '',
      ''
    );

    this.userService.getUser(this.id).subscribe(
      data => this.user = data,
    );
  }

  editUser() {
    this.userService.updateUser(this.id, this.user).subscribe(
      data => {
        console.log(data);
        this.router.navigate(['user/list']);

      }
    );
  }

}
