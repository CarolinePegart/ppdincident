import { Component, OnInit } from '@angular/core';
import {Subscription} from 'rxjs';
import {Router} from '@angular/router';
import {UserDataService} from '../../service/user-data.service';

@Component({
  selector: 'app-user-list',
  templateUrl: './user-list.component.html',
  styleUrls: ['./user-list.component.css']
})
export class UserListComponent implements OnInit {

  userList = [];
  private usersSubscription: Subscription;
  private message: string;

  constructor(private userService: UserDataService, private router: Router) { }

  ngOnInit() {
    this.usersSubscription = this.userService.usersSubject.subscribe(
      (users: any) => {
        this.userList = users['hydra:member'];
      }
    );
    this.userService.getUserList();


  }

  createUser() {
    // Renvoie sur la page du formulaire pour créer un utilisateur
    this.router.navigate(['user/add']);

  }

  deleteUser(id) {
    console.log(`delete user ${id}`);
    this.userService.deleteUser(id).subscribe(
      response => {
        console.log(response);
        this.message = `Suppression réussie`;
        this.userService.getUserList();
      }
    );
  }

}
