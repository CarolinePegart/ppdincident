import { Component, OnInit } from '@angular/core';
import {User} from '../../model/user.model';
import {UserDataService} from '../../service/user-data.service';
import {Router} from '@angular/router';

@Component({
  selector: 'app-user-add',
  templateUrl: './user-add.component.html',
  styleUrls: ['./user-add.component.css']
})
export class UserAddComponent implements OnInit {

  user: User;
  id: number;

  constructor(private userService: UserDataService, private router: Router) { }

  ngOnInit() {
    this.user = new User(
      this.id,
      '',
      '',
      '',
      '',
      '',
      '',
    );
  }

  createUser() {
    this.userService.postUser(this.user).subscribe(
      data => {
        console.log(data);
        this.router.navigate(['user/list']);
      }
    );
  }

}
