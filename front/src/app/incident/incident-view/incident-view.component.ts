import { Component, OnInit } from '@angular/core';
import { Incident } from '../../model/incident.model';
import { IncidentDataService } from '../../service/incident-data.service';
import { ActivatedRoute } from '@angular/router';
import {Subscription} from 'rxjs';
import {CustomerDataService} from '../../service/customer-data.service';

@Component({
  selector: 'app-incident-view',
  templateUrl: './incident-view.component.html',
  styleUrls: ['./incident-view.component.css']
})
export class IncidentViewComponent implements OnInit {

  id: number;
  incident: Incident;
  customerList = [];
  private customersSubscription: Subscription;


  constructor(private incidentService: IncidentDataService, private route: ActivatedRoute, private customerService: CustomerDataService) { }

  ngOnInit() {
    this.id = this.route.snapshot.params.id;
    this.incident = new Incident(this.id,
      '',
      '',
      new Date(),
      new Date(),
      '',
      '',
      '',
      null,
      '',
      null,
      new Date(),
      new Date(),
      new Date(),
      '',
      '',
      null,
      '',
      '',
      '',
      '',
      '');

    this.incidentService.getIncident(this.id).subscribe(
       data => {
         this.incident = data;
         console.log(this.incident);
       }

     );
    console.log(this.incident);
    this.customersSubscription = this.customerService.customersSubject.subscribe(
      (customers: any) => {
        this.customerList = customers['hydra:member'];
        console.log(this.customerList);

      }
    );
    this.customerService.getCustomerList();
  }

}
