import { Component, OnInit} from '@angular/core';
import { IncidentDataService } from '../../service/incident-data.service';
import { Incident } from '../../model/incident.model';
import { Router } from '@angular/router';
import { CustomerDataService } from 'src/app/service/customer-data.service';
import { Subscription } from 'rxjs';

@Component({
  selector: 'app-incident-add',
  templateUrl: './incident-add.component.html',
  styleUrls: ['./incident-add.component.css']
})

export class IncidentAddComponent implements OnInit {

  incident: Incident;
  id: number;
  idIncident: any;

  customerList = [];
  private customersSubscription: Subscription;


  constructor(private incidentService: IncidentDataService, private router: Router, private customerService: CustomerDataService) { }

  ngOnInit() {
    this.incidentService.currentIncidentNumber.subscribe(idIncident =>
      this.idIncident = idIncident);


    this.incident = new Incident(this.id,
      `INC`.concat(this.idIncident + 1),
      '',
      new Date(),
      new Date(),
      '',
      '',
      '',
      null,
      '',
      null,
      new Date(),
      new Date(),
      new Date(),
      '',
      '',
      null,
      '',
      '',
      '',
      '',
      '');

    this.customersSubscription = this.customerService.customersSubject.subscribe(
        (customers: any) => {
            this.customerList = customers['hydra:member'];
            console.log(this.customerList);

        }
    );
    this.customerService.getCustomerList();
  }

  createIncident() {
    this.incidentService.postIncident(this.incident).subscribe(
      data => {
        console.log(data);
        this.router.navigate(['incident/list']);
      }
    );
  }





}
