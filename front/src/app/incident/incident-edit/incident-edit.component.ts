import { Component, OnInit } from '@angular/core';
import { IncidentDataService } from '../../service/incident-data.service';
import { Router, ActivatedRoute } from '@angular/router';
import { Incident } from '../../model/incident.model';
import {Subscription} from 'rxjs';
import {CustomerDataService} from '../../service/customer-data.service';

@Component({
  selector: 'app-incident-edit',
  templateUrl: './incident-edit.component.html',
  styleUrls: ['./incident-edit.component.css']
})
export class IncidentEditComponent implements OnInit {

  id: number;
  incident: Incident;

  customerList = [];
  private customersSubscription: Subscription;



  // tslint:disable-next-line:max-line-length
  constructor(private incidentService: IncidentDataService, private customerService: CustomerDataService, private route: ActivatedRoute, private router: Router) { }

  ngOnInit() {
    this.id = this.route.snapshot.params.id;
    this.incident = new Incident(this.id,
      '',
      '',
      new Date(),
      new Date(),
      '',
      '',
      '',
      null,
      '',
      null,
      new Date(),
      new Date(),
      new Date(),
      '',
      '',
      null,
      '',
      '',
      '',
      '',
      ''
      );

    this.incidentService.getIncident(this.id).subscribe(
       data => {
         this.incident = data;
         console.log(data);
       },

     );
    console.log(this.incident);

    this.customersSubscription = this.customerService.customersSubject.subscribe(
      (customers: any) => {
        this.customerList = customers['hydra:member'];
        console.log(this.customerList);

      }
    );
    this.customerService.getCustomerList();
  }

  editIncident() {
    this.incidentService.updateIncident(this.id, this.incident).subscribe(
      data => {
        console.log(data);
        this.router.navigate(['incident/list']);

      }
    );
  }

}
