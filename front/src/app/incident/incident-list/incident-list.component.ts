import { Component, OnInit } from '@angular/core';
import { IncidentDataService } from '../../service/incident-data.service';
import {Subscription} from 'rxjs';
import { Router } from '@angular/router';

@Component({
  selector: 'app-incident-list',
  templateUrl: './incident-list.component.html',
  styleUrls: ['./incident-list.component.css']
})
export class IncidentListComponent implements OnInit {

  incidentList = [];
  lastIdIncidentList: any;


  private incidentsSubscription: Subscription;

  constructor(private incidentService: IncidentDataService, private router: Router) { }

  ngOnInit() {
      this.incidentsSubscription = this.incidentService.incidentsSubject.subscribe(
          (incidents: any) => {
              this.incidentList = incidents['hydra:member'];

              this.lastIdIncidentList = this.incidentList[this.incidentList.length - 1].id;
              console.log(this.lastIdIncidentList);

          }
      );
      this.incidentService.getIncidentList();

      this.incidentService.currentIncidentNumber.subscribe(lastIdIncidentList =>
        this.lastIdIncidentList = lastIdIncidentList);
  }

  createIncident() {
    // Envoie le dernier id de table incident au component add-incident
    this.incidentService.majIncidentNumber(this.lastIdIncidentList);
    // Renvoie sur la page du formulaire pour créer un incident
    this.router.navigate(['incident/add']);

  }



}
