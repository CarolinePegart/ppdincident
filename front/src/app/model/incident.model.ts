export class Incident {
    constructor(
      public id: number,
      public incidentNumber: string,
      public incidentStatus: string,
      public registeredDate: Date,
      public resolutionDate: Date,
      public incidentTopic: string,
      public incidentPriority: string,
      public incidentDescription: string,
      public externalNumberIncident: number,
      public material: string,
      public severity: number,
      public startDateP0P1: Date,
      public endDateP0P1: Date,
      public expectedRecoveryDate: Date,
      public incidentType: string,
      public blockingSituation: string,
      public nbUsersInvolved: number,
      public securityRisk: string,
      public situationDetails: string,
      public securityDetails: string,
      public incidentHistorical: string,
      public customer: string
      ) {}
  }
