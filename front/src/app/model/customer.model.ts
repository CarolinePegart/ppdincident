export class Customer {
  constructor(
    public id: number,
    public customerLastName: string,
    public customerFirstName: string,
    public customerService: string,
    public customerLocation: string,
    public customerPhoneNumber: string,
  ) {}
}
