export class Problem {
  constructor(
    public id: number,
    public problemNumber: string,
    public registeredDate: Date,
    public problemStatus: string,
    public problemTopic: string,
    public problemPriority: string,
    public knowErrorRelated: string,
    public problemDescription: string,
    public blockingSituation: string,
    public incidents: string,
    public resolutionMaximumDate: Date,

  ) {}
}
