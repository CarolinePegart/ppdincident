import { Component, OnInit } from '@angular/core';
import {Subscription} from 'rxjs';
import {CustomerDataService} from '../../service/customer-data.service';
import {Router} from '@angular/router';

@Component({
  selector: 'app-customer-list',
  templateUrl: './customer-list.component.html',
  styleUrls: ['./customer-list.component.css']
})
export class CustomerListComponent implements OnInit {

  customerList = [];
  private customersSubscription: Subscription;
  private message: string;

  constructor(private customerService: CustomerDataService, private router: Router) { }

  ngOnInit() {
    this.customersSubscription = this.customerService.customersSubject.subscribe(
      (customers: any) => {
        this.customerList = customers['hydra:member'];
      }
    );
    this.customerService.getCustomerList();


  }



  createCustomer() {
    // Renvoie sur la page du formulaire pour créer un client
    this.router.navigate(['customer/add']);

  }

  deleteCustomer(id) {
    console.log(`delete customer ${id}`);
    this.customerService.deleteCustomer(id).subscribe(
      response => {
        console.log(response);
        this.message = `Suppression réussie`;
        this.customerService.getCustomerList();
      }
    );
  }


}
