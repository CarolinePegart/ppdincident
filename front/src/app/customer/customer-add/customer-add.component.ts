import { Component, OnInit } from '@angular/core';
import {Customer} from '../../model/customer.model';
import {CustomerDataService} from '../../service/customer-data.service';
import {Router} from '@angular/router';


@Component({
  selector: 'app-customer-add',
  templateUrl: './customer-add.component.html',
  styleUrls: ['./customer-add.component.css']
})
export class CustomerAddComponent implements OnInit {
  customer: Customer;
  id: number;

  constructor(private customerService: CustomerDataService, private router: Router) {
  }

  ngOnInit() {
    this.customer = new Customer(
      this.id,
      '',
      '',
      '',
      '',
      ''
    );
  }

  createCustomer() {
    this.customerService.postCustomer(this.customer).subscribe(
      data => {
        console.log(data);
        this.router.navigate(['customer/list']);
      }
    );
  }

}
