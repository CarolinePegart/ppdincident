import { Component, OnInit } from '@angular/core';
import {Customer} from '../../model/customer.model';
import {CustomerDataService} from '../../service/customer-data.service';
import {ActivatedRoute, Router} from '@angular/router';

@Component({
  selector: 'app-customer-edit',
  templateUrl: './customer-edit.component.html',
  styleUrls: ['./customer-edit.component.css']
})
export class CustomerEditComponent implements OnInit {
  customer: Customer;
  id: number;

  constructor(private customerService: CustomerDataService, private router: Router, private route: ActivatedRoute) {}

  ngOnInit() {
    this.id = this.route.snapshot.params.id;
    this.customer = new Customer(
      this.id,
      '',
      '',
      '',
      '',
      ''
    );
    this.customerService.getCustomer(this.id).subscribe(
      data => this.customer = data,

    );
    console.log(this.customer);
  }

  editCustomer() {
    this.customerService.updateCustomer(this.id, this.customer).subscribe(
      data => {
        console.log(data);
        this.router.navigate(['customer/list']);

      }
    );
  }

}
