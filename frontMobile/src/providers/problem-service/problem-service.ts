import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Subject, BehaviorSubject } from 'rxjs';
import { Problem } from '../../models/Problem';

/*
  Generated class for the ProblemServiceProvider provider.

  See https://angular.io/guide/dependency-injection for more info on providers
  and Angular DI.
*/
@Injectable()
export class ProblemServiceProvider {

  private URL_BACKEND = 'http://127.0.0.1:8000/api';

  problemsSubject = new Subject<any>();

  problemList = [];

  private problemNumber = new BehaviorSubject('');
  currentProblemNumber = this.problemNumber.asObservable();

  constructor(private http: HttpClient) { }

  emitProblemsSubject() {
    this.problemsSubject.next(this.problemList);
  }

  getProblemList() {
    this.http.get(`${this.URL_BACKEND}/problems`).subscribe((response: any) => {
      this.problemList = response;
      console.log(response);
      this.emitProblemsSubject();
    });
  }

  majProblemNumber(message: string) {
    this.problemNumber.next(message);
  }

  // create problem (back)
  postProblem(problem) {
    return this.http.post(`${this.URL_BACKEND}/problems`, problem);
  }

  // get problem (back)
  getProblem(id) {
    return this.http.get<Problem>(`${this.URL_BACKEND}/problems/${id}`);
  }

  // update problem (back)
  updateProblem(id, problem) {
    return this.http.put(`${this.URL_BACKEND}/problems/${id}`, problem);
  }

}
