import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Subject, BehaviorSubject } from 'rxjs';
import { Request } from '../../models/Request';

/*
  Generated class for the RequestServiceProvider provider.

  See https://angular.io/guide/dependency-injection for more info on providers
  and Angular DI.
*/
@Injectable()
export class RequestServiceProvider {

  private URL_BACKEND = 'http://127.0.0.1:8000/api';

  requestsSubject = new Subject<any>();
  requestList = [];

  private requestNumber = new BehaviorSubject('');
  currentRequestNumber = this.requestNumber.asObservable();


  constructor(private http: HttpClient) {
  }

  emitRequestsSubject() {
    this.requestsSubject.next(this.requestList);
  }

  majRequestNumber(message: string) {
    this.requestNumber.next(message);
  }

  getRequestList() {
    this.http.get(`${this.URL_BACKEND}/requests`).subscribe((response: any) => {
      this.requestList = response;
      console.log(response);
      this.emitRequestsSubject();
    });
  }

  // create request (back)
  postRequest(request) {
    return this.http.post(`${this.URL_BACKEND}/requests`, request);
  }

  // get request (back)
  getRequest(id) {
    return this.http.get<Request>(`${this.URL_BACKEND}/requests/${id}`);
  }

  // update request (back)
  updateRequest(id, request) {
    return this.http.put(`${this.URL_BACKEND}/requests/${id}`, request);
  }

}
