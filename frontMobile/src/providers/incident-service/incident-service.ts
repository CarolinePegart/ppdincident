import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Subject, BehaviorSubject } from 'rxjs';
import { Incident } from '../../models/Incident';

/*
  Generated class for the IncidentServiceProvider provider.

  See https://angular.io/guide/dependency-injection for more info on providers
  and Angular DI.
*/
@Injectable()
export class IncidentServiceProvider {

  private URL_BACKEND = 'http://127.0.0.1:8000/api';

  incidentsSubject = new Subject<any>();

  incidentList = [];

  private incidentNumber = new BehaviorSubject('');
  currentIncidentNumber = this.incidentNumber.asObservable();

  constructor(private http: HttpClient) { }

    emitIncidentsSubject() {
        this.incidentsSubject.next(this.incidentList);
    }

    getIncidentList() {
        this.http.get(`${this.URL_BACKEND}/incidents`).subscribe((response: any) => {
            this.incidentList = response;
            console.log(response);
            this.emitIncidentsSubject();
        });
    }

    majIncidentNumber(message: string) {
      this.incidentNumber.next(message);
    }

  // create incident (back)
    postIncident(incident) {
    return this.http.post(`${this.URL_BACKEND}/incidents`, incident);
  }

  // get incident (back)
  getIncident(id) {
    return this.http.get<Incident>(`${this.URL_BACKEND}/incidents/${id}`);
  }

  // update incident (back)
  updateIncident(id, incident) {
    return this.http.put(`${this.URL_BACKEND}/incidents/${id}`, incident);
  }

}
