import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Subject } from 'rxjs';
import { Customer } from '../../models/Customer';


/*
  Generated class for the CustomerServiceProvider provider.

  See https://angular.io/guide/dependency-injection for more info on providers
  and Angular DI.
*/
@Injectable()
export class CustomerServiceProvider {

  private URL_BACKEND = 'http://127.0.0.1:8000/api';

  customersSubject = new Subject<any>();

  customerList = [];



  constructor(private http: HttpClient) { }

  emitCustomersSubject() {
    this.customersSubject.next(this.customerList);
  }

  getCustomerList() {
    this.http.get(`${this.URL_BACKEND}/customers`).subscribe((response: any) => {
      this.customerList = response;
      console.log(response);
      this.emitCustomersSubject();
    });
  }

  // create customer (back)
  postCustomer(customer) {
    return this.http.post(`${this.URL_BACKEND}/customers`, customer);
  }

  // get customer (back)
  getCustomer(id) {
    return this.http.get<Customer>(`${this.URL_BACKEND}/customers/${id}`);
  }

  // update the customer (back)
  updateCustomer(id, customer) {
    return this.http.put(`${this.URL_BACKEND}/customers/${id}`, customer);
  }

  // Remove the customer
  deleteCustomer(id) {
    return this.http.delete(`${this.URL_BACKEND}/customers/${id}`);
  }

}
