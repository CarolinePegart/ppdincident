import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { ChangesViewPage } from './changes-view';

@NgModule({
  declarations: [
    ChangesViewPage,
  ],
  imports: [
    IonicPageModule.forChild(ChangesViewPage),
  ],
})
export class ChangesViewPageModule {}
