import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';
import { Change } from '../../models/Change';
import { ChangeServiceProvider } from '../../providers/change-service/change-service';

/**
 * Generated class for the ChangesViewPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-changes-view',
  templateUrl: 'changes-view.html',
})
export class ChangesViewPage {
  change: Change;
  id: number;

  constructor(public navCtrl: NavController, public navParams: NavParams, public changeService: ChangeServiceProvider) {
  }

  ngOnInit() {
    this.id = this.navParams.get('id');

    this.change = new Change(this.id,
      '',
      new Date(),
      '',
      '',
      '',
      '',
      '',
      '',
      '',
      new Date(),
      new Date(),
      '',
      '',
      '',
      '',
      '',
      '',
      new Date(),
      new Date(),
      new Date(),
      new Date(),
      new Date()
    );

    this.changeService.getChange(this.id).subscribe(
      data => {
        this.change = data;
        console.log(this.change);
      }
    );
  }


}
