import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';
import { Problem } from '../../models/Problem';
import { ProblemServiceProvider } from '../../providers/problem-service/problem-service';

/**
 * Generated class for the ProblemViewPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-problem-view',
  templateUrl: 'problem-view.html',
})
export class ProblemViewPage {

  id: number;
  problem: Problem;

  constructor(public navCtrl: NavController, 
              public navParams: NavParams,
              public problemService: ProblemServiceProvider) {
  }

  ngOnInit() {
    this.id = this.navParams.get('id');

    this.problem = new Problem(
      this.id,
      '',
       new Date(),
      '',
      '',
      '',
      '',
      '',
      '',
      '',
      new Date()
    );

    this.problemService.getProblem(this.id).subscribe(
      data => {
        this.problem = data;
        console.log(this.problem);
      }
    );
  }

}
