import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { ProblemViewPage } from './problem-view';

@NgModule({
  declarations: [
    ProblemViewPage,
  ],
  imports: [
    IonicPageModule.forChild(ProblemViewPage),
  ],
})
export class ProblemViewPageModule {}
