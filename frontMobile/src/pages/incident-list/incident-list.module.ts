import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { IncidentListPage } from './incident-list';

@NgModule({
  declarations: [
    IncidentListPage,
  ],
  imports: [
    IonicPageModule.forChild(IncidentListPage),
  ],
})
export class IncidentListPageModule {}
