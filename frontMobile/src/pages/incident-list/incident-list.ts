import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';
import { Subscription } from 'rxjs';
import { IncidentServiceProvider } from '../../providers/incident-service/incident-service';
import { IncidentViewPage } from '../incident-view/incident-view';

/**
 * Generated class for the IncidentListPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-incident-list',
  templateUrl: 'incident-list.html',
})
export class IncidentListPage {
  incidentList = [];
  lastIdIncidentList: any;
  private incidentsSubscription: Subscription;
  pushPage:any;

  constructor(public navCtrl: NavController, 
              public navParams: NavParams,
              public incidentService: IncidentServiceProvider) {
                this.pushPage= IncidentViewPage;
                
                this.incidentsSubscription = this.incidentService.incidentsSubject.subscribe(
                  (incidents: any) => {
                      this.incidentList = incidents['hydra:member'];
        
                      this.lastIdIncidentList = this.incidentList[this.incidentList.length - 1].id;
                      console.log(this.lastIdIncidentList);
        
                  }
              );
              this.incidentService.getIncidentList();
        
              this.incidentService.currentIncidentNumber.subscribe(lastIdIncidentList =>
                this.lastIdIncidentList = lastIdIncidentList);
              }

  viewIncident(id:any) {
    this.navCtrl.push(IncidentViewPage,{id: id});
  }

}
