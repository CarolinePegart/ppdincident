import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { IncidentViewPage } from './incident-view';

@NgModule({
  declarations: [
    IncidentViewPage,
  ],
  imports: [
    IonicPageModule.forChild(IncidentViewPage),
  ],
})
export class IncidentViewPageModule {}
