import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';
import { Subscription } from 'rxjs';
import { IncidentServiceProvider } from '../../providers/incident-service/incident-service';
import { CustomerServiceProvider } from '../../providers/customer-service/customer-service';
import { Incident } from '../../models/Incident';

/**
 * Generated class for the IncidentViewPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-incident-view',
  templateUrl: 'incident-view.html',
})
export class IncidentViewPage {

  id: number;
  incident: Incident;
  customerList = [];
  private customersSubscription: Subscription;

  constructor(public navCtrl: NavController, 
              public navParams: NavParams,
              public incidentService: IncidentServiceProvider,
              public customerService: CustomerServiceProvider) {
              
            
               
              }

              ngOnInit() {
                this.id = this.navParams.get('id');
                this.incident = new Incident(this.id,
                  '',
                  '',
                  new Date(),
                  new Date(),
                  '',
                  '',
                  '',
                  null,
                  '',
                  null,
                  new Date(),
                  new Date(),
                  new Date(),
                  '',
                  '',
                  null,
                  '',
                  '',
                  '',
                  '',
                  '');
            
                this.incidentService.getIncident(this.id).subscribe(
                   data => {
                     this.incident = data;
                     console.log(this.incident);
                   }
            
                 );
                console.log(this.incident);
                this.customersSubscription = this.customerService.customersSubject.subscribe(
                  (customers: any) => {
                    this.customerList = customers['hydra:member'];
                    console.log(this.customerList);
            
                  }
                );
                this.customerService.getCustomerList();
              }
  

  

}
