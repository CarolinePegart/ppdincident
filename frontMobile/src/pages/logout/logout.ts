import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams, Events } from 'ionic-angular';
import { LoginPage } from '../login/login';
import { AuthServiceProvider } from '../../providers/auth-service/auth-service';

/**
 * Generated class for the LogoutPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-logout',
  templateUrl: 'logout.html',
})
export class LogoutPage {

  constructor(public navCtrl: NavController, public navParams: NavParams, events:Events, public authenticationService: AuthServiceProvider) {
    events.publish('user:loggedout');
    navCtrl.setRoot(LoginPage);
    authenticationService.logout();
   }

  ionViewDidLoad() {
    console.log('ionViewDidLoad LogoutPage');
  }

}
