import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { ChangesListPage } from './changes-list';

@NgModule({
  declarations: [
    ChangesListPage,
  ],
  imports: [
    IonicPageModule.forChild(ChangesListPage),
  ],
})
export class ChangesListPageModule {}
