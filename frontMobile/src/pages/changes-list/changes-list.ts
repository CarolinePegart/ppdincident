import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';
import { Subscription } from 'rxjs';
import { ChangeServiceProvider } from '../../providers/change-service/change-service';
import { ChangesViewPage } from '../changes-view/changes-view';

/**
 * Generated class for the ChangesListPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-changes-list',
  templateUrl: 'changes-list.html',
})
export class ChangesListPage {

  changeList = [];
  lastIdChangeList: any;


  private changesSubscription: Subscription;
  
  constructor(public navCtrl: NavController, 
              public navParams: NavParams,
              public changeService: ChangeServiceProvider) {

                this.changesSubscription = this.changeService.changesSubject.subscribe(
                  (changes: any) => {
                    this.changeList = changes['hydra:member'];
            
                    this.lastIdChangeList = this.changeList[this.changeList.length - 1].id;
                    console.log(this.lastIdChangeList);
            
                  }
                );
                this.changeService.getChangeList();
            
                this.changeService.currentChangeNumber.subscribe(lastIdChangeList =>
                  this.lastIdChangeList = lastIdChangeList);
  }

  viewChange(id:any) {
    this.navCtrl.push(ChangesViewPage,{id: id});
  }

}
