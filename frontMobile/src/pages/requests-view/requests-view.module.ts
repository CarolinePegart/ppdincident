import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { RequestsViewPage } from './requests-view';

@NgModule({
  declarations: [
    RequestsViewPage,
  ],
  imports: [
    IonicPageModule.forChild(RequestsViewPage),
  ],
})
export class RequestsViewPageModule {}
