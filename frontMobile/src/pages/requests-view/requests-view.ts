import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';
import { Subscription } from 'rxjs';
import { Request } from '../../models/Request';
import { RequestServiceProvider } from '../../providers/request-service/request-service';
import { CustomerServiceProvider } from '../../providers/customer-service/customer-service';

/**
 * Generated class for the RequestsViewPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-requests-view',
  templateUrl: 'requests-view.html',
})
export class RequestsViewPage {

  id: number;
  request: Request;
  customerList = [];
  private customersSubscription: Subscription;

  constructor(public navCtrl: NavController, 
              public navParams: NavParams,
              public requestService: RequestServiceProvider,
              public customerService: CustomerServiceProvider) {
  }

  ngOnInit() {
    this.id = this.navParams.get('id');

    this.request = new Request(
      this.id,
      '',
      new Date(),
      '',
      '',
      '',
      '',
      '',
      '',
      '',
      '',
      '',
      '',
      '',
      '',
      '',
      new Date(),
      new Date()
    );

    this.requestService.getRequest(this.id).subscribe(
      data => {
        this.request = data;
        console.log(this.request);
      }
    );

    this.customersSubscription = this.customerService.customersSubject.subscribe(
      (customers: any) => {
        this.customerList = customers['hydra:member'];
        console.log(this.customerList);

      }
    );
    this.customerService.getCustomerList();
  }

}
