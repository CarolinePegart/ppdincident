import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';
import { Subscription } from 'rxjs';
import { RequestServiceProvider } from '../../providers/request-service/request-service';
import { RequestsViewPage } from '../requests-view/requests-view';

/**
 * Generated class for the RequestsListPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-requests-list',
  templateUrl: 'requests-list.html',
})
export class RequestsListPage {

  requestList = [];
  lastIdRequestList: any;


  private requestsSubscription: Subscription;

  constructor(public navCtrl: NavController, 
              public navParams: NavParams,
              public requestService: RequestServiceProvider) {

                this.requestsSubscription = this.requestService.requestsSubject.subscribe(
                  (requests: any) => {
                    this.requestList = requests['hydra:member'];
            
                    this.lastIdRequestList = this.requestList[this.requestList.length - 1].id;
                    console.log(this.lastIdRequestList);
            
                  }
                );
                this.requestService.getRequestList();
            
                this.requestService.currentRequestNumber.subscribe(lastIdRequestList =>
                  this.lastIdRequestList = lastIdRequestList);
  }

  viewRequest(id:any) {
    this.navCtrl.push(RequestsViewPage,{id: id});
  }

}
