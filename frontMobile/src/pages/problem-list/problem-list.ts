import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';
import { Subscription } from 'rxjs';
import { ProblemServiceProvider } from '../../providers/problem-service/problem-service';
import { ProblemViewPage } from '../problem-view/problem-view';

/**
 * Generated class for the ProblemListPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-problem-list',
  templateUrl: 'problem-list.html',
})
export class ProblemListPage {
  problemList = [];
  lastIdProblemList: any;

  private problemsSubscription: Subscription;


  constructor(public navCtrl: NavController, 
              public navParams: NavParams,
              public problemService: ProblemServiceProvider
              ) {
                this.problemsSubscription = this.problemService.problemsSubject.subscribe(
                  (problems: any) => {
                    this.problemList = problems['hydra:member'];
            
                    this.lastIdProblemList = this.problemList[this.problemList.length - 1].id;
                    console.log(this.lastIdProblemList);
            
                  }
                );
                this.problemService.getProblemList();
            
                this.problemService.currentProblemNumber.subscribe(
                  lastIdProblemList => this.lastIdProblemList = lastIdProblemList
                );
  }

  viewProblem(id:any) {
    this.navCtrl.push(ProblemViewPage,{id: id});
  }

}
