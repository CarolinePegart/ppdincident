export class Request {
    constructor(
      public id: number,
      public requestNumber: string,
      public registeredDate: Date,
      public requestStatus: string,
      public requestLabel: string,
      public customer: string,
      public requestPriority: string,
      public externCallNumber: string,
      public form: string,
      public impactedPopulation: string,
      public requestMaterialCode: string,
      public parent: string,
      public requestEmergencyUser: string,
      public justification: string,
      public requestDescription: string,
      public blockingSituation: string,
      public deliveryDate: Date,
      public endRequestDate: Date,
  
  
    ) {}
  }
  