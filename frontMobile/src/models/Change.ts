export class Change {
    constructor(
      public id: number,
      public changeNumber: string,
      public registeredDate: Date,
      public changeStatus: string,
      public changeLabel: string,
      public changePriority: string,
      public changeCallNumber: string,
      public changeLocation: string,
      public origin: string,
      public impactedPopulation: string,
      public issueDateRequest: Date,
      public closingDateRequest: Date,
      public changeMaterialCode: string,
      public customerLabel: string,
      public userEmergency: string,
      public environment: string,
      public changeDescription: string,
      public requestTracking: string,
      public changeStartDate: Date,
      public changeEndDate: Date,
      public plannedStartDate: Date,
      public plannedEndDate: Date,
      public cabDate: Date
  
  
    ) {}
  }
  