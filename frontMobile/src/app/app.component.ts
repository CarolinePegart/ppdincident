import { Component, ViewChild } from '@angular/core';
import { Platform, Nav, Events } from 'ionic-angular';
import { StatusBar } from '@ionic-native/status-bar';
import { SplashScreen } from '@ionic-native/splash-screen';

import { HomePage } from '../pages/home/home';
import { LoginPage } from '../pages/login/login';
import { IncidentListPage } from '../pages/incident-list/incident-list';
import { InfoPage } from '../pages/info/info';
import { ProblemListPage } from '../pages/problem-list/problem-list';
import { RequestsListPage } from '../pages/requests-list/requests-list';
import { ChangesListPage } from '../pages/changes-list/changes-list';
import { LogoutPage } from '../pages/logout/logout';
@Component({
  templateUrl: 'app.html'
})
export class MyApp {
  @ViewChild(Nav) nav: Nav;

  rootPage:any = LoginPage;
  pages: Array<{title: string, component: any}>;

  constructor(platform: Platform, statusBar: StatusBar, splashScreen: SplashScreen, events:Events) {
    platform.ready().then(() => {
      // Okay, so the platform is ready and our plugins are available.
      // Here you can do any higher level native things you might need.
      statusBar.styleDefault();
      splashScreen.hide();
    });

    this.pages = [
      {title:'Connexion', component: LoginPage},
      {title:'Informations', component: InfoPage},
      
      ];

    events.subscribe('user:loggedin',()=>{
      this.pages = [
                    
                    {title:'Incident', component: IncidentListPage},
                    {title:'Problems', component: ProblemListPage},
                    {title:'Request', component: RequestsListPage},
                    {title:'Changes', component: ChangesListPage},
                    {title:'Déconnexion', component: LogoutPage},
                    
                    ];
    });

      events.subscribe('user:loggedout',()=>{
      this.pages = [
                    {title:'Connexion', component: LoginPage},
                    {title:'Informations', component: InfoPage},

                    
                    
                    ];
    });
  }

  openPage(page) {
    // Reset the content nav to have just this page
    // we wouldn't want the back button to show in this scenario
    this.nav.setRoot(page.component);
  }
}

