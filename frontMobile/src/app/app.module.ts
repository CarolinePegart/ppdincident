import { BrowserModule } from '@angular/platform-browser';
import { ErrorHandler, NgModule } from '@angular/core';
import { IonicApp, IonicErrorHandler, IonicModule } from 'ionic-angular';
import { SplashScreen } from '@ionic-native/splash-screen';
import { StatusBar } from '@ionic-native/status-bar';

import { MyApp } from './app.component';
import { HomePage } from '../pages/home/home';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { HttpClientModule, HTTP_INTERCEPTORS } from '@angular/common/http';
import { LoginPage } from '../pages/login/login';
import { InfoPage } from '../pages/info/info';
import { IncidentListPage } from '../pages/incident-list/incident-list';
import { IncidentViewPage } from '../pages/incident-view/incident-view';
import { ProblemListPage } from '../pages/problem-list/problem-list';
import { ProblemViewPage } from '../pages/problem-view/problem-view';
import { ChangesListPage } from '../pages/changes-list/changes-list';
import { ChangesViewPage } from '../pages/changes-view/changes-view';
import { RequestsListPage } from '../pages/requests-list/requests-list';
import { RequestsViewPage } from '../pages/requests-view/requests-view';
import { AuthServiceProvider } from '../providers/auth-service/auth-service';
import { LogoutPage } from '../pages/logout/logout';
import { JwtInterceptor } from '../providers/jwt-interceptor/jwt-interceptor';
import { IncidentServiceProvider } from '../providers/incident-service/incident-service';
import { CustomerServiceProvider } from '../providers/customer-service/customer-service';
import { ProblemServiceProvider } from '../providers/problem-service/problem-service';
import { ChangeServiceProvider } from '../providers/change-service/change-service';
import { RequestServiceProvider } from '../providers/request-service/request-service';

@NgModule({
  declarations: [
    MyApp,
    HomePage,
    LoginPage,
    InfoPage,
    IncidentListPage,
    IncidentViewPage,
    ProblemListPage,
    ProblemViewPage,
    ChangesListPage,
    ChangesViewPage,
    RequestsListPage,
    RequestsViewPage,
    LogoutPage
  ],
  imports: [
    BrowserModule,
    IonicModule.forRoot(MyApp),
    HttpClientModule,
    FormsModule,
    ReactiveFormsModule
  ],
  bootstrap: [IonicApp],
  entryComponents: [
    MyApp,
    HomePage,
    LoginPage,
    InfoPage,
    IncidentListPage,
    IncidentViewPage,
    ProblemListPage,
    ProblemViewPage,
    ChangesListPage,
    ChangesViewPage,
    RequestsListPage,
    RequestsViewPage,
    LogoutPage
  ],
  providers: [
    StatusBar,
    SplashScreen,
    {provide: ErrorHandler, useClass: IonicErrorHandler},
    AuthServiceProvider,
    { provide: HTTP_INTERCEPTORS, useClass: JwtInterceptor, multi: true },
    IncidentServiceProvider,
    CustomerServiceProvider,
    ProblemServiceProvider,
    ChangeServiceProvider,
    RequestServiceProvider,
  ]
})
export class AppModule {}
