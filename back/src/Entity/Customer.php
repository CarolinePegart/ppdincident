<?php

namespace App\Entity;

use ApiPlatform\Core\Annotation\ApiSubresource;
use Doctrine\Common\Collections\Collection;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\ORM\Mapping as ORM;
use ApiPlatform\Core\Annotation\ApiResource;
use Symfony\Component\Serializer\Annotation\Groups;

/**
 *@ApiResource(
 * )
 *@ORM\Entity(repositoryClass="App\Repository\CustomerRepository")
 */
class Customer
{
    /**
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer")
     *
     *
     */
    private $id;

    /**
     * @ORM\Column(type="string", length=255)
     * @Groups({"get-collection-information-customer"})
     */
    private $customerFirstName;

    /**
     * @ORM\Column(type="string", length=255)
     * @Groups({"get-collection-information-customer"})
     *
     */
    private $customerLastName;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $customerLocation;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $customerPhoneNumber;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $customerService;

    /**
     * @ORM\OneToMany(targetEntity="App\Entity\Incident", mappedBy="customer")
     * @ApiSubresource()
     *
     */
    private $incidents;

    /**
     * @ORM\OneToMany(targetEntity="Requests", mappedBy="customer")
     * @ApiSubresource()
     *
     */
    private $requests;

    /**
     * @return Collection
     */
    public function getRequests(): Collection
    {
        return $this->requests;
    }

    /**
     * @return Collection
     */
    public function getIncidents(): Collection
    {
        return $this->incidents;
    }




    public function __construct()
    {
       $this->incidents = new ArrayCollection();
       $this->requests = new ArrayCollection();

    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getCustomerFirstName(): ?string
    {
        return $this->customerFirstName;
    }

    public function setCustomerFirstName(string $customerFirstName): self
    {
        $this->customerFirstName = $customerFirstName;

        return $this;
    }

    public function getCustomerLastName(): ?string
    {
        return $this->customerLastName;
    }

    public function setCustomerLastName(string $customerLastName): self
    {
        $this->customerLastName = $customerLastName;

        return $this;
    }

    public function getCustomerLocation(): ?string
    {
        return $this->customerLocation;
    }

    public function setCustomerLocation(?string $customerLocation): self
    {
        $this->customerLocation = $customerLocation;

        return $this;
    }

    public function getCustomerPhoneNumber(): ?string
    {
        return $this->customerPhoneNumber;
    }

    public function setCustomerPhoneNumber(?string $customerPhoneNumber): self
    {
        $this->customerPhoneNumber = $customerPhoneNumber;

        return $this;
    }

    public function getCustomerService(): ?string
    {
        return $this->customerService;
    }

    public function setCustomerService(?string $customerService): self
    {
        $this->customerService = $customerService;

        return $this;
    }





}
