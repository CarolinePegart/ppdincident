<?php

namespace App\Entity;
interface DateEntityInterface
{
    public function setRegisteredDate(\DateTimeInterface $registeredDate): DateEntityInterface;
}