<?php

namespace App\Entity;

use Doctrine\Common\Collections\Collection;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\ORM\Mapping as ORM;
use ApiPlatform\Core\Annotation\ApiResource;
use ApiPlatform\Core\Annotation\ApiSubresource;
use Symfony\Component\Serializer\Annotation\Groups;

/**
 * @ApiResource(
 *     itemOperations={
 *         "get"={
 *             "normalization_context"={
 *                 "groups"={"get-problem"}
 *             }
 *         },
 *         "put"={
 *             "normalization_context"={
 *                 "groups"={"get-problem"}
 *             }
 *         }
 *     },
 *     collectionOperations={
 *         "post"={
 *             "normalization_context"={
 *                 "groups"={"get-problem"}
 *             }
 *         },
 *         "get"={
 *             "normalization_context"={
 *                 "groups"={"get-problem","get-collection-information-user"}
 *             }
 *         },
 *
 *     }
 * )
 * @ORM\Entity(repositoryClass="App\Repository\ProblemRepository")
 */
class Problem implements DateEntityInterface
{
    /**
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer")
     * @Groups({"get-problem"})
     */
    private $id;

    /**
     * @ORM\Column(type="string", length=255)
     * @Groups({"get-problem"})
     */
    private $problemNumber;

    /**
     * @ORM\Column(type="date")
     * @Groups({"get-problem"})
     */
    private $registeredDate;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     * @Groups({"get-problem"})
     */
    private $problemStatus;

    /**
     * @ORM\Column(type="string", nullable=true)
     * @Groups({"get-problem"})
     */
    private $problemPriority;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     * @Groups({"get-problem"})
     */
    private $problemTopic;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     * @Groups({"get-problem"})
     */
    private $problemDescription;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     * @Groups({"get-problem"})
     */
    private $knownErrorRelated;

    /**
     * @ORM\Column(type="date")
     * @Groups({"get-problem"})
     */
    private $resolutionMaximumDate;



    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     * @Groups({"get-problem"})
     */
    private $incidents;

    /**
     * @ORM\ManyToOne(targetEntity="App\Entity\User", inversedBy="problems")
     * @ORM\JoinColumn(nullable=false)
     * @Groups({"get-problem"})
     */
    private $user;



    public function __construct()
    {


    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getProblemNumber(): ?string
    {
        return $this->problemNumber;
    }

    public function setProblemNumber(string $problemNumber): self
    {
        $this->problemNumber = $problemNumber;

        return $this;
    }

    /**
     * @return mixed
     */
    public function getRegisteredDate()
    {
        return $this->registeredDate;
    }


    public function setRegisteredDate(\DateTimeInterface $registeredDate): DateEntityInterface
    {
        $this->registeredDate = $registeredDate;

        return $this;
    }

    public function getProblemStatus(): ?string
    {
        return $this->problemStatus;
    }

    public function setProblemStatus(string $problemStatus): self
    {
        $this->problemStatus = $problemStatus;

        return $this;
    }

    public function getProblemPriority(): ?string
    {
        return $this->problemPriority;
    }

    public function setProblemPriority(string $problemPriority): self
    {
        $this->problemPriority = $problemPriority;

        return $this;
    }

    public function getProblemTopic(): ?string
    {
        return $this->problemTopic;
    }

    public function setProblemTopic(?string $problemTopic): self
    {
        $this->problemTopic = $problemTopic;

        return $this;
    }

    public function getProblemDescription(): ?string
    {
        return $this->problemDescription;
    }

    public function setProblemDescription(?string $problemDescription): self
    {
        $this->problemDescription = $problemDescription;

        return $this;
    }

    public function getKnownErrorRelated(): ?string
    {
        return $this->knownErrorRelated;
    }

    public function setKnownErrorRelated(?string $knownErrorRelated): self
    {
        $this->knownErrorRelated = $knownErrorRelated;

        return $this;
    }


    public function getResolutionMaximumDate()
    {
        return $this->resolutionMaximumDate;
    }


    public function setResolutionMaximumDate($resolutionMaximumDate): void
    {
        $this->resolutionMaximumDate = $resolutionMaximumDate;
    }


    public function getIncidents()
    {
        return $this->incidents;
    }


    public function setIncidents($incidents): void
    {
        $this->incidents = $incidents;
    }

    /**
     * @return User
     */
    public function getUser()
    {
        return $this->user;
    }

    /**
     * @param User $user
     */
    public function setUser(User $user): self
    {
        $this->user = $user;

        return $this;
    }





}
