<?php

namespace App\Entity;

use Doctrine\ORM\Mapping as ORM;
use ApiPlatform\Core\Annotation\ApiResource;
use Symfony\Component\Serializer\Annotation\Groups;

/**
 * @ApiResource(
 *       itemOperations={
 *         "get"={
 *             "normalization_context"={
 *                 "groups"={"get-request-with-customer"}
 *             }
 *         },
 *         "put"={
 *             "normalization_context"={
 *                 "groups"={"get-request-with-customer"}
 *             }
 *         }
 *     },
 *     collectionOperations={
 *         "post"={
 *             "normalization_context"={
 *                 "groups"={"get-request-with-customer"}
 *             }
 *         },
 *         "get"={
 *             "normalization_context"={
 *                 "groups"={"get-request-with-customer","get-collection-information-customer","get-collection-information-user"}
 *             }
 *         },
 *
 *     }
 * )
 * @ORM\Entity(repositoryClass="App\Repository\RequestRepository")
 */
class Requests implements DateEntityInterface
{
    /**
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer")
     * @Groups({"get-request-with-customer"})
     */
    private $id;

    /**
     * @ORM\Column(type="string", length=255)
     * @Groups({"get-request-with-customer"})
     */
    private $requestNumber;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     * @Groups({"get-request-with-customer"})
     */
    private $externalCallNumber;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     * @Groups({"get-request-with-customer"})
     */
    private $requestStatus;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     * @Groups({"get-request-with-customer"})
     */
    private $parent;

    /**
     * @ORM\Column(type="date")
     * @Groups({"get-request-with-customer"})
     */
    private $registeredDate;

    /**
     * @ORM\Column(type="date", nullable=true)
     * @Groups({"get-request-with-customer"})
     */
    private $deliveryDate;

    /**
     * @ORM\Column(type="date", nullable=true)
     * @Groups({"get-request-with-customer"})
     */
    private $endRequestDate;


    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     * @Groups({"get-request-with-customer"})
     */
    private $requestEmergencyUser;

    /**
     * @ORM\Column(type="string",length=255, nullable=true)
     * @Groups({"get-request-with-customer"})
     */
    private $requestPriority;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     * @Groups({"get-request-with-customer"})
     */
    private $requestMaterialCode;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     * @Groups({"get-request-with-customer"})
     */
    private $justification;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     * @Groups({"get-request-with-customer"})
     */
    private $form;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     * @Groups({"get-request-with-customer"})
     */
    private $requestLabel;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     * @Groups({"get-request-with-customer"})
     */
    private $blockingSituation;


    /**
     * @ORM\ManyToOne(targetEntity="App\Entity\Customer",inversedBy="requests")
     * @ORM\JoinColumn(nullable=true)
     * @Groups({"get-request-with-customer"})
     */
    private $customer;

    /**
     * @ORM\ManyToOne(targetEntity="App\Entity\User", inversedBy="requests")
     * @ORM\JoinColumn(nullable=false)
     * @Groups({"get-request-with-customer"})
     */
    private $user;

    /**
     * @return Customer
     */
    public function getCustomer(): Customer
    {
        return $this->customer;
    }

    /**
     * @param Customer $customer
     */
    public function setCustomer(Customer $customer): Customer
    {
        return $this->customer = $customer;

    }



    public function getId(): ?int
    {
        return $this->id;
    }

    public function getRequestNumber(): ?string
    {
        return $this->requestNumber;
    }

    public function setRequestNumber(string $requestNumber): self
    {
        $this->requestNumber = $requestNumber;

        return $this;
    }

    public function getExternalCallNumber(): ?string
    {
        return $this->externalCallNumber;
    }

    public function setExternalCallNumber(?string $externalCallNumber): self
    {
        $this->externalCallNumber = $externalCallNumber;

        return $this;
    }


    public function getParent(): ?string
    {
        return $this->parent;
    }

    public function setParent(?string $parent): self
    {
        $this->parent = $parent;

        return $this;
    }

    public function getRegisteredDate(): ?\DateTimeInterface
    {
        return $this->registeredDate;
    }

    public function setRegisteredDate(\DateTimeInterface $registeredDate): DateEntityInterface
    {
        $this->registeredDate = $registeredDate;

        return $this;
    }

    public function getDeliveryDate(): ?\DateTimeInterface
    {
        return $this->deliveryDate;
    }

    public function setDeliveryDate(?\DateTimeInterface $deliveryDate): self
    {
        $this->deliveryDate = $deliveryDate;

        return $this;
    }

    public function getEndRequestDate()
    {
        return $this->endRequestDate;
    }


    public function setEndRequestDate($endRequestDate): self
    {
        $this->endRequestDate = $endRequestDate;

        return $this;
    }

    public function getRequestEmergencyUser(): ?string
    {
        return $this->requestEmergencyUser;
    }

    public function setRequestEmergencyUser(?string $requestEmergencyUser): self
    {
        $this->requestEmergencyUser = $requestEmergencyUser;

        return $this;
    }

    public function getRequestPriority(): ?string
    {
        return $this->requestPriority;
    }

    public function setRequestPriority(?string $requestPriority): self
    {
        $this->requestPriority = $requestPriority;

        return $this;
    }

    public function getRequestMaterialCode(): ?string
    {
        return $this->requestMaterialCode;
    }

    public function setRequestMaterialCode(?string $requestMaterialCode): self
    {
        $this->requestMaterialCode = $requestMaterialCode;

        return $this;
    }

    public function getJustification(): ?string
    {
        return $this->justification;
    }

    public function setJustification(?string $justification): self
    {
        $this->justification = $justification;

        return $this;
    }

    public function getForm(): ?string
    {
        return $this->form;
    }

    public function setForm(?string $form): self
    {
        $this->form = $form;

        return $this;
    }

    public function getRequestLabel(): ?string
    {
        return $this->requestLabel;
    }

    public function setRequestLabel(?string $requestLabel): self
    {
        $this->requestLabel = $requestLabel;

        return $this;
    }

    /**
     * @return mixed
     */
    public function getRequestStatus()
    {
        return $this->requestStatus;
    }

    /**
     * @param mixed $requestStatus
     */
    public function setRequestStatus($requestStatus): void
    {
        $this->requestStatus = $requestStatus;
    }

    /**
     * @return mixed
     */
    public function getBlockingSituation()
    {
        return $this->blockingSituation;
    }

    /**
     * @param mixed $blockingSituation
     */
    public function setBlockingSituation($blockingSituation): void
    {
        $this->blockingSituation = $blockingSituation;
    }

    /**
     * @return User
     */
    public function getUser()
    {
        return $this->user;
    }

    /**
     * @param User $user
     */
    public function setUser(User $user): self
    {
        $this->user = $user;

        return $this;
    }



}
