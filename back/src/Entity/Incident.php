<?php

namespace App\Entity;


use Doctrine\ORM\Mapping as ORM;
use ApiPlatform\Core\Annotation\ApiResource;
use Symfony\Component\Serializer\Annotation\Groups;

/**
  * @ApiResource(
  *     itemOperations={
  *         "get"={
  *             "normalization_context"={
  *                 "groups"={"get-incident-with-customer"}
  *             }
  *         },
  *         "put"={
  *             "normalization_context"={
  *                 "groups"={"get-incident-with-customer"}
  *             }
  *         }
  *     },
  *     collectionOperations={
  *         "post"={
  *             "normalization_context"={
  *                 "groups"={"get-incident-with-customer"}
  *             }
  *         },
  *         "get"={
  *             "normalization_context"={
  *                 "groups"={"get-incident-with-customer","get-collection-information-customer", "get-collection-information-user"}
  *             }
  *         },
  *
  *     }
  * )
  *
  *
  * @ORM\Entity(repositoryClass="App\Repository\IncidentRepository")
  */
class Incident implements DateEntityInterface
{
    /**
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer")
     * @Groups({"get-incident-with-customer"})
     */
    private $id;

    /**
     * @ORM\Column(type="string", length=255)
     * @Groups({"get-incident-with-customer"})
     */
    private $incidentNumber;

    /**
     * @ORM\Column(type="string", length=255)
     * @Groups({"get-incident-with-customer"})
     */
    private $incidentStatus;

    /**
     * @ORM\Column(type="date")
     * @Groups({"get-incident-with-customer"})
     */
    private $registeredDate;

    /**
     * @ORM\Column(type="date", nullable=true)
     * @Groups({"get-incident-with-customer"})
     */
    private $resolutionDate;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     * @Groups({"get-incident-with-customer"})
     */
    private $incidentTopic;

    /**
     * @ORM\Column(type="string")
     * @Groups({"get-incident-with-customer"})
     */
    private $incidentPriority;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     * @Groups({"get-incident-with-customer"})
     */
    private $incidentDescription;

    /**
     * @ORM\Column(type="integer", nullable=true)
     * @Groups({"get-incident-with-customer"})
     */
    private $externalNumberIncident;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     * @Groups({"get-incident-with-customer"})
     */
    private $material;

    /**
     * @ORM\Column(type="string")
     * @Groups({"get-incident-with-customer"})
     */
    private $severity;

    /**
     * @ORM\Column(type="date", nullable=true)
     * @Groups({"get-incident-with-customer"})
     */
    private $startDateP0P1;

    /**
     * @ORM\Column(type="date", nullable=true)
     * @Groups({"get-incident-with-customer"})
     */
    private $endDateP0P1;

    /**
     * @ORM\Column(type="date", nullable=true)
     * @Groups({"get-incident-with-customer"})
     */
    private $expectedRecoveryDate;

    /**
     * @ORM\Column(type="string", length=255)
     * @Groups({"get-incident-with-customer"})
     */
    private $incidentType;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     * @Groups({"get-incident-with-customer"})
     */
    private $blockingSituation;

    /**
     * @ORM\Column(type="string", nullable=true)
     * @Groups({"get-incident-with-customer"})
     */
    private $nbUsersInvolved;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     * @Groups({"get-incident-with-customer"})
     */
    private $securityRisk;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     * @Groups({"get-incident-with-customer"})
     */
    private $situationDetails;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     * @Groups({"get-incident-with-customer"})
     */
    private $securityDetails;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     * @Groups({"get-incident-with-customer"})
     */
    private $incidentHistorical;

    /**
     * @ORM\ManyToOne(targetEntity="App\Entity\Customer",inversedBy="incidents")
     * @ORM\JoinColumn(nullable=true)
     * @Groups({"get-incident-with-customer","get-collection-information-customer"})
     */
    private $customer;

    /**
     * @ORM\ManyToOne(targetEntity="App\Entity\User", inversedBy="incidents")
     * @ORM\JoinColumn(nullable=false)
     * @Groups({"get-incident-with-customer"})
     */
    private $user;



    /**
     * @return Customer
     */
    public function getCustomer(): Customer
    {
        return $this->customer;
    }

    /**
     * @param Customer $customer
     */
    public function setCustomer(Customer $customer): Customer
    {
       return $this->customer = $customer;

    }

    /**
     * @return mixed
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @param mixed $id
     */
    public function setId($id): void
    {
        $this->id = $id;
    }

    /**
     * @return mixed
     */
    public function getIncidentNumber()
    {
        return $this->incidentNumber;
    }

    /**
     * @param mixed $incidentNumber
     */
    public function setIncidentNumber($incidentNumber): void
    {
        $this->incidentNumber = $incidentNumber;
    }

    /**
     * @return mixed
     */
    public function getIncidentStatus()
    {
        return $this->incidentStatus;
    }

    /**
     * @param mixed $incidentStatus
     */
    public function setIncidentStatus($incidentStatus): void
    {
        $this->incidentStatus = $incidentStatus;
    }

    public function getRegisteredDate(): ?\DateTimeInterface
    {
        return $this->registeredDate;
    }

    public function setRegisteredDate(\DateTimeInterface $registeredDate): DateEntityInterface
    {
        $this->registeredDate = $registeredDate;

        return $this;
    }

    public function getResolutionDate(): ?\DateTimeInterface
    {
        return $this->resolutionDate;
    }

    public function setResolutionDate(?\DateTimeInterface $resolutionDate): self
    {
        $this->resolutionDate = $resolutionDate;

        return $this;
    }

    /**
     * @return mixed
     */
    public function getIncidentTopic()
    {
        return $this->incidentTopic;
    }

    /**
     * @param mixed $incidentTopic
     */
    public function setIncidentTopic($incidentTopic): void
    {
        $this->incidentTopic = $incidentTopic;
    }

    /**
     * @return mixed
     */
    public function getIncidentPriority()
    {
        return $this->incidentPriority;
    }

    /**
     * @param mixed $incidentPriority
     */
    public function setIncidentPriority($incidentPriority): void
    {
        $this->incidentPriority = $incidentPriority;
    }

    /**
     * @return mixed
     */
    public function getIncidentDescription()
    {
        return $this->incidentDescription;
    }

    /**
     * @param mixed $incidentDescription
     */
    public function setIncidentDescription($incidentDescription): void
    {
        $this->incidentDescription = $incidentDescription;
    }

    /**
     * @return mixed
     */
    public function getExternalNumberIncident()
    {
        return $this->externalNumberIncident;
    }

    /**
     * @param mixed $externalNumberIncident
     */
    public function setExternalNumberIncident($externalNumberIncident): void
    {
        $this->externalNumberIncident = $externalNumberIncident;
    }

    /**
     * @return mixed
     */
    public function getMaterial()
    {
        return $this->material;
    }

    /**
     * @param mixed $material
     */
    public function setMaterial($material): void
    {
        $this->material = $material;
    }

    /**
     * @return mixed
     */
    public function getSeverity()
    {
        return $this->severity;
    }

    /**
     * @param mixed $severity
     */
    public function setSeverity($severity): void
    {
        $this->severity = $severity;
    }

    public function getStartDateP0P1(): ?\DateTimeInterface
    {
        return $this->startDateP0P1;
    }

    public function setStartDateP0P1(?\DateTimeInterface $startDateP0P1): self
    {
        $this->startDateP0P1 = $startDateP0P1;

        return $this;
    }

    public function getEndDateP0P1(): ?\DateTimeInterface
    {
        return $this->endDateP0P1;
    }

    public function setEndDateP0P1(?\DateTimeInterface $endDateP0P1): self
    {
        $this->endDateP0P1 = $endDateP0P1;

        return $this;
    }

    public function getExpectedRecoveryDate(): ?\DateTimeInterface
    {
        return $this->expectedRecoveryDate;
    }

    public function setExpectedRecoveryDate(?\DateTimeInterface $expectedRecoveryDate): self
    {
        $this->expectedRecoveryDate = $expectedRecoveryDate;

        return $this;
    }

    /**
     * @return mixed
     */
    public function getIncidentType()
    {
        return $this->incidentType;
    }

    /**
     * @param mixed $incidentType
     */
    public function setIncidentType($incidentType): void
    {
        $this->incidentType = $incidentType;
    }

    /**
     * @return mixed
     */
    public function getBlockingSituation()
    {
        return $this->blockingSituation;
    }

    /**
     * @param mixed $blockingSituation
     */
    public function setBlockingSituation($blockingSituation): void
    {
        $this->blockingSituation = $blockingSituation;
    }

    /**
     * @return mixed
     */
    public function getNbUsersInvolved()
    {
        return $this->nbUsersInvolved;
    }

    /**
     * @param mixed $nbUsersInvolved
     */
    public function setNbUsersInvolved($nbUsersInvolved): void
    {
        $this->nbUsersInvolved = $nbUsersInvolved;
    }

    /**
     * @return mixed
     */
    public function getSecurityRisk()
    {
        return $this->securityRisk;
    }

    /**
     * @param mixed $securityRisk
     */
    public function setSecurityRisk($securityRisk): void
    {
        $this->securityRisk = $securityRisk;
    }

    /**
     * @return mixed
     */
    public function getSituationDetails()
    {
        return $this->situationDetails;
    }

    /**
     * @param mixed $situationDetails
     */
    public function setSituationDetails($situationDetails): void
    {
        $this->situationDetails = $situationDetails;
    }

    /**
     * @return mixed
     */
    public function getSecurityDetails()
    {
        return $this->securityDetails;
    }

    /**
     * @param mixed $securityDetails
     */
    public function setSecurityDetails($securityDetails): void
    {
        $this->securityDetails = $securityDetails;
    }

    /**
     * @return mixed
     */
    public function getIncidentHistorical()
    {
        return $this->incidentHistorical;
    }

    /**
     * @param mixed $incidentHistorical
     */
    public function setIncidentHistorical($incidentHistorical): void
    {
        $this->incidentHistorical = $incidentHistorical;
    }

    /**
     * @return User
     */
    public function getUser()
    {
        return $this->user;
    }

    /**
     * @param User $user
     */
    public function setUser(User $user): self
    {
        $this->user = $user;

        return $this;
    }





}
