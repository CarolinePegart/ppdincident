<?php

namespace App\Entity;

use Doctrine\ORM\Mapping as ORM;
use ApiPlatform\Core\Annotation\ApiResource;
use Symfony\Component\Serializer\Annotation\Groups;

/**
 *@ApiResource(
 *     itemOperations={
 *         "get"={
 *             "normalization_context"={
 *                 "groups"={"get-changes"}
 *             }
 *         },
 *         "put"={
 *             "normalization_context"={
 *                 "groups"={"get-changes"}
 *             }
 *         }
 *     },
 *     collectionOperations={
 *         "post"={
 *             "normalization_context"={
 *                 "groups"={"get-changes"}
 *             }
 *         },
 *         "get"={
 *             "normalization_context"={
 *                 "groups"={"get-changes","get-collection-information-user"}
 *             }
 *         },
 *
 *     }
 * )
 * @ORM\Entity(repositoryClass="App\Repository\ChangesRepository")
 */
class Changes implements DateEntityInterface
{
    /**
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer")
     * @Groups({"get-changes"})
     */
    private $id;

    /**
     * @ORM\Column(type="string", length=255)
     * @Groups({"get-changes"})
     */
    private $changeNumber;

    /**
     * @ORM\Column(type="date")
     * @Groups({"get-changes"})
     */
    private $registeredDate;

    /**
     * @ORM\Column(type="string", length=255)
     * @Groups({"get-changes"})
     */
    private $changeStatus;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     * @Groups({"get-changes"})
     */
    private $changeCallNumber;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     * @Groups({"get-changes"})
     */
    private $origin;

    /**
     * @ORM\Column(type="date", nullable=true)
     * @Groups({"get-changes"})
     */
    private $issueDateRequest;

    /**
     * @ORM\Column(type="date", nullable=true)
     * @Groups({"get-changes"})
     */
    private $closingDateRequest;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     * @Groups({"get-changes"})
     */
    private $changeMaterialCode;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     * @Groups({"get-changes"})
     */
    private $configurationItemName;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     * @Groups({"get-changes"})
     */
    private $customerLabel;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     * @Groups({"get-changes"})
     */
    private $changeLabel;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     * @Groups({"get-changes"})
     */
    private $changeLocation;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     * @Groups({"get-changes"})
     */
    private $userEmergency;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     * @Groups({"get-changes"})
     */
    private $environment;

    /**
     * @ORM\Column(type="string", nullable=true)
     * @Groups({"get-changes"})
     */
    private $changePriority;

    /**
     * @ORM\Column(type="string", nullable=true)
     * @Groups({"get-changes"})
     */
    private $impactedPopulation;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     * @Groups({"get-changes"})
     */
    private $changeDescription;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     * @Groups({"get-changes"})
     */
    private $requestTracking;

    /**
     * @ORM\Column(type="date", nullable=true)
     * @Groups({"get-changes"})
     */
    private $changeStartDate;

    /**
     * @ORM\Column(type="date", nullable=true)
     * @Groups({"get-changes"})
     */
    private $plannedStartDate;

    /**
     * @ORM\Column(type="date", nullable=true)
     * @Groups({"get-changes"})
     */
    private $cabDate;

    /**
     * @ORM\Column(type="date", nullable=true)
     * @Groups({"get-changes"})
     */
    private $changeEndDate;

    /**
     * @ORM\Column(type="date", nullable=true)
     * @Groups({"get-changes"})
     */
    private $plannedEndDate;

    /**
     * @ORM\Column(type="date", nullable=true)
     * @Groups({"get-changes"})
     */
    private $actualEndDate;

    /**
     * @ORM\ManyToOne(targetEntity="App\Entity\User", inversedBy="changes")
     * @ORM\JoinColumn(nullable=false)
     * @Groups({"get-changes"})
     */
    private $user;

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getChangeNumber(): ?string
    {
        return $this->changeNumber;
    }

    public function setChangeNumber(string $changeNumber): self
    {
        $this->changeNumber = $changeNumber;

        return $this;
    }

    public function getRegisteredDate(): ?\DateTimeInterface
    {
        return $this->registeredDate;
    }

    public function setRegisteredDate(\DateTimeInterface $registeredDate): DateEntityInterface
    {
        $this->registeredDate = $registeredDate;

        return $this;
    }

    public function getChangeStatus(): ?string
    {
        return $this->changeStatus;
    }

    public function setChangeStatus(string $changeStatus): self
    {
        $this->changeStatus = $changeStatus;

        return $this;
    }

    public function getChangeCallNumber(): ?string
    {
        return $this->changeCallNumber;
    }

    public function setChangeCallNumber(?string $changeCallNumber): self
    {
        $this->changeCallNumber = $changeCallNumber;

        return $this;
    }

    public function getOrigin(): ?string
    {
        return $this->origin;
    }

    public function setOrigin(?string $origin): self
    {
        $this->origin = $origin;

        return $this;
    }

    public function getIssueDateRequest(): ?\DateTimeInterface
    {
        return $this->issueDateRequest;
    }

    public function setIssueDateRequest(?\DateTimeInterface $issueDateRequest): self
    {
        $this->issueDateRequest = $issueDateRequest;

        return $this;
    }

    public function getClosingDateRequest(): ?\DateTimeInterface
    {
        return $this->closingDateRequest;
    }

    public function setClosingDateRequest(?\DateTimeInterface $closingDateRequest): self
    {
        $this->closingDateRequest = $closingDateRequest;

        return $this;
    }

    public function getChangeMaterialCode(): ?string
    {
        return $this->changeMaterialCode;
    }

    public function setChangeMaterialCode(?string $changeMaterialCode): self
    {
        $this->changeMaterialCode = $changeMaterialCode;

        return $this;
    }

    public function getConfigurationItemName(): ?string
    {
        return $this->configurationItemName;
    }

    public function setConfigurationItemName(?string $configurationItemName): self
    {
        $this->configurationItemName = $configurationItemName;

        return $this;
    }

    public function getCustomerLabel(): ?string
    {
        return $this->customerLabel;
    }

    public function setCustomerLabel(?string $customerLabel): self
    {
        $this->customerLabel = $customerLabel;

        return $this;
    }

    public function getChangeLabel(): ?string
    {
        return $this->changeLabel;
    }

    public function setChangeLabel(?string $changeLabel): self
    {
        $this->changeLabel = $changeLabel;

        return $this;
    }

    public function getChangeLocation(): ?string
    {
        return $this->changeLocation;
    }

    public function setChangeLocation(?string $changeLocation): self
    {
        $this->changeLocation = $changeLocation;

        return $this;
    }

    public function getUserEmergency(): ?string
    {
        return $this->userEmergency;
    }

    public function setUserEmergency(?string $userEmergency): self
    {
        $this->userEmergency = $userEmergency;

        return $this;
    }

    public function getEnvironment(): ?string
    {
        return $this->environment;
    }

    public function setEnvironment(?string $environment): self
    {
        $this->environment = $environment;

        return $this;
    }

    public function getChangePriority(): ?string
    {
        return $this->changePriority;
    }

    public function setChangePriority(?string $changePriority): self
    {
        $this->changePriority = $changePriority;

        return $this;
    }

    public function getChangeDescription(): ?string
    {
        return $this->changeDescription;
    }

    public function setChangeDescription(?string $changeDescription): self
    {
        $this->changeDescription = $changeDescription;

        return $this;
    }

    public function getRequestTracking(): ?string
    {
        return $this->requestTracking;
    }

    public function setRequestTracking(?string $requestTracking): self
    {
        $this->requestTracking = $requestTracking;

        return $this;
    }

    public function getChangeStartDate(): ?\DateTimeInterface
    {
        return $this->changeStartDate;
    }

    public function setChangeStartDate(?\DateTimeInterface $changeStartDate): self
    {
        $this->changeStartDate = $changeStartDate;

        return $this;
    }

    public function getPlannedStartDate(): ?\DateTimeInterface
    {
        return $this->plannedStartDate;
    }

    public function setPlannedStartDate(?\DateTimeInterface $plannedStartDate): self
    {
        $this->plannedStartDate = $plannedStartDate;

        return $this;
    }

    public function getCabDate(): ?\DateTimeInterface
    {
        return $this->cabDate;
    }

    public function setCabDate(?\DateTimeInterface $cabDate): self
    {
        $this->cabDate = $cabDate;

        return $this;
    }

    public function getChangeEndDate(): ?\DateTimeInterface
    {
        return $this->changeEndDate;
    }

    public function setChangeEndDate(?\DateTimeInterface $changeEndDate): self
    {
        $this->changeEndDate = $changeEndDate;

        return $this;
    }

    public function getPlannedEndDate(): ?\DateTimeInterface
    {
        return $this->plannedEndDate;
    }

    public function setPlannedEndDate(?\DateTimeInterface $plannedEndDate): self
    {
        $this->plannedEndDate = $plannedEndDate;

        return $this;
    }

    public function getActualEndDate(): ?\DateTimeInterface
    {
        return $this->actualEndDate;
    }

    public function setActualEndDate(?\DateTimeInterface $actualEndDate): self
    {
        $this->actualEndDate = $actualEndDate;

        return $this;
    }

    /**
     * @return mixed
     */
    public function getImpactedPopulation()
    {
        return $this->impactedPopulation;
    }

    /**
     * @param mixed $impactedPopulation
     */
    public function setImpactedPopulation($impactedPopulation): void
    {
        $this->impactedPopulation = $impactedPopulation;
    }

    /**
     * @return User
     */
    public function getUser()
    {
        return $this->user;
    }

    /**
     * @param User $user
     */
    public function setUser(User $user): self
    {
        $this->user = $user;

        return $this;
    }


}
