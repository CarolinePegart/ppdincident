<?php


namespace App\EventSubscriber;

use ApiPlatform\Core\EventListener\EventPriorities;
use App\Entity\DateEntityInterface;
use App\Entity\RegisteredDateEntityInterfaced;
use Symfony\Component\EventDispatcher\EventSubscriberInterface;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpKernel\Event\GetResponseForControllerResultEvent;
use Symfony\Component\HttpKernel\KernelEvents;

class RegisteredDateEntity implements EventSubscriberInterface
{
    public static function getSubscribedEvents()
    {
        return [
            KernelEvents::VIEW => ['setDateRegistered', EventPriorities::PRE_WRITE]
        ];
    }

    public function setDateRegistered(GetResponseForControllerResultEvent $event)
    {
        $entity = $event->getControllerResult();
        $method = $event->getRequest()->getMethod();
        if (!$entity instanceof DateEntityInterface|| Request::METHOD_POST !== $method) {
            return;
        }
        $entity->setRegisteredDate(new \DateTime());
    }
}