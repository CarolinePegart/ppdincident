<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20190504145257 extends AbstractMigration
{
    public function getDescription() : string
    {
        return '';
    }

    public function up(Schema $schema) : void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('ALTER TABLE `change` ADD change_call_number VARCHAR(255) DEFAULT NULL, ADD issue_date_request DATE DEFAULT NULL, ADD change_material_code VARCHAR(255) DEFAULT NULL, ADD configuration_item_name VARCHAR(255) DEFAULT NULL, ADD change_location VARCHAR(255) DEFAULT NULL, ADD user_emergency VARCHAR(255) DEFAULT NULL, ADD environment VARCHAR(255) DEFAULT NULL, ADD change_description VARCHAR(255) DEFAULT NULL, ADD request_tracking VARCHAR(255) DEFAULT NULL, ADD change_start_date DATE DEFAULT NULL, ADD planned_start_date DATE DEFAULT NULL, ADD change_end_date DATE DEFAULT NULL, ADD planned_end_date DATE DEFAULT NULL, ADD actual_end_date DATE DEFAULT NULL, DROP call_externalnumber, DROP send_date_request, DROP material_code_request, DROP indentification_name, DROP location_change, DROP emergency_user, DROP environnement, DROP description_change, DROP follow_request, DROP begin_date_change, DROP begin_date_planned, DROP end_date_change, DROP end_date_planned, DROP end_date_real, CHANGE status_change change_status VARCHAR(255) NOT NULL, CHANGE priority_change change_priority INT DEFAULT NULL');
        $this->addSql('ALTER TABLE customer ADD customer_first_name VARCHAR(255) NOT NULL, ADD customer_last_name VARCHAR(255) NOT NULL, ADD customer_location VARCHAR(255) DEFAULT NULL, ADD customer_phone_number VARCHAR(255) DEFAULT NULL, ADD customer_service VARCHAR(255) DEFAULT NULL, DROP name_customer, DROP lastname_customer, DROP location_customer, DROP phone_number_customer, DROP service_customer');
        $this->addSql('ALTER TABLE incident ADD incident_status VARCHAR(255) NOT NULL, ADD resolution_date DATE DEFAULT NULL, ADD incident_topic VARCHAR(255) DEFAULT NULL, ADD incident_description VARCHAR(255) DEFAULT NULL, ADD start_date_p0p1 DATE DEFAULT NULL, ADD expected_recovery_date DATE DEFAULT NULL, ADD incident_type VARCHAR(255) NOT NULL, ADD incident_historical VARCHAR(255) DEFAULT NULL, DROP statut_incident, DROP date_resolution, DROP topic_incident, DROP description_incident, DROP begin_date_p0p1, DROP expected_date_recovery, DROP type_incident, DROP historical_incident, CHANGE date_save_incident incident_registration_date DATE NOT NULL, CHANGE priority_incident incident_priority INT NOT NULL, CHANGE n_incident_extern external_number_incident INT DEFAULT NULL');
        $this->addSql('ALTER TABLE problem ADD problem_topic VARCHAR(255) DEFAULT NULL, ADD problem_description VARCHAR(255) DEFAULT NULL, ADD known_error_related VARCHAR(255) DEFAULT NULL, DROP subject_problem, DROP description_problem, DROP known_error, CHANGE priority_problem problem_priority INT NOT NULL');
        $this->addSql('ALTER TABLE request ADD external_status VARCHAR(255) DEFAULT NULL, ADD request_emergency_user VARCHAR(255) DEFAULT NULL, ADD request_material_code VARCHAR(255) DEFAULT NULL, ADD form VARCHAR(255) DEFAULT NULL, DROP emergency_user_request, DROP material_code, DROP formulaire, CHANGE date_save_request request_registration_date DATE NOT NULL, CHANGE priority_request request_priority INT NOT NULL');
    }

    public function down(Schema $schema) : void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('ALTER TABLE `change` ADD call_externalnumber VARCHAR(255) DEFAULT NULL COLLATE utf8mb4_unicode_ci, ADD send_date_request DATE DEFAULT NULL, ADD material_code_request VARCHAR(255) DEFAULT NULL COLLATE utf8mb4_unicode_ci, ADD indentification_name VARCHAR(255) DEFAULT NULL COLLATE utf8mb4_unicode_ci, ADD location_change VARCHAR(255) DEFAULT NULL COLLATE utf8mb4_unicode_ci, ADD emergency_user VARCHAR(255) DEFAULT NULL COLLATE utf8mb4_unicode_ci, ADD environnement VARCHAR(255) DEFAULT NULL COLLATE utf8mb4_unicode_ci, ADD description_change VARCHAR(255) DEFAULT NULL COLLATE utf8mb4_unicode_ci, ADD follow_request VARCHAR(255) DEFAULT NULL COLLATE utf8mb4_unicode_ci, ADD begin_date_change DATE DEFAULT NULL, ADD begin_date_planned DATE DEFAULT NULL, ADD end_date_change DATE DEFAULT NULL, ADD end_date_planned DATE DEFAULT NULL, ADD end_date_real DATE DEFAULT NULL, DROP change_call_number, DROP issue_date_request, DROP change_material_code, DROP configuration_item_name, DROP change_location, DROP user_emergency, DROP environment, DROP change_description, DROP request_tracking, DROP change_start_date, DROP planned_start_date, DROP change_end_date, DROP planned_end_date, DROP actual_end_date, CHANGE change_status status_change VARCHAR(255) NOT NULL COLLATE utf8mb4_unicode_ci, CHANGE change_priority priority_change INT DEFAULT NULL');
        $this->addSql('ALTER TABLE customer ADD name_customer VARCHAR(255) NOT NULL COLLATE utf8mb4_unicode_ci, ADD lastname_customer VARCHAR(255) NOT NULL COLLATE utf8mb4_unicode_ci, ADD location_customer VARCHAR(255) DEFAULT NULL COLLATE utf8mb4_unicode_ci, ADD phone_number_customer VARCHAR(255) DEFAULT NULL COLLATE utf8mb4_unicode_ci, ADD service_customer VARCHAR(255) DEFAULT NULL COLLATE utf8mb4_unicode_ci, DROP customer_first_name, DROP customer_last_name, DROP customer_location, DROP customer_phone_number, DROP customer_service');
        $this->addSql('ALTER TABLE incident ADD statut_incident VARCHAR(255) NOT NULL COLLATE utf8mb4_unicode_ci, ADD date_resolution DATE DEFAULT NULL, ADD topic_incident VARCHAR(255) DEFAULT NULL COLLATE utf8mb4_unicode_ci, ADD description_incident VARCHAR(255) DEFAULT NULL COLLATE utf8mb4_unicode_ci, ADD begin_date_p0p1 DATE DEFAULT NULL, ADD expected_date_recovery DATE DEFAULT NULL, ADD type_incident VARCHAR(255) NOT NULL COLLATE utf8mb4_unicode_ci, ADD historical_incident VARCHAR(255) DEFAULT NULL COLLATE utf8mb4_unicode_ci, DROP incident_status, DROP resolution_date, DROP incident_topic, DROP incident_description, DROP start_date_p0p1, DROP expected_recovery_date, DROP incident_type, DROP incident_historical, CHANGE incident_registration_date date_save_incident DATE NOT NULL, CHANGE incident_priority priority_incident INT NOT NULL, CHANGE external_number_incident n_incident_extern INT DEFAULT NULL');
        $this->addSql('ALTER TABLE problem ADD subject_problem VARCHAR(255) DEFAULT NULL COLLATE utf8mb4_unicode_ci, ADD description_problem VARCHAR(255) DEFAULT NULL COLLATE utf8mb4_unicode_ci, ADD known_error VARCHAR(255) DEFAULT NULL COLLATE utf8mb4_unicode_ci, DROP problem_topic, DROP problem_description, DROP known_error_related, CHANGE problem_priority priority_problem INT NOT NULL');
        $this->addSql('ALTER TABLE request ADD emergency_user_request VARCHAR(255) DEFAULT NULL COLLATE utf8mb4_unicode_ci, ADD material_code VARCHAR(255) DEFAULT NULL COLLATE utf8mb4_unicode_ci, ADD formulaire VARCHAR(255) DEFAULT NULL COLLATE utf8mb4_unicode_ci, DROP external_status, DROP request_emergency_user, DROP request_material_code, DROP form, CHANGE request_registration_date date_save_request DATE NOT NULL, CHANGE request_priority priority_request INT NOT NULL');
    }
}
