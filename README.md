#Application ITSM

##Pr�requis

* [Composer](https://getcomposer.org/)
* [NPM](https://www.npmjs.com/)
* [WampServer](http://www.wampserver.com/)

##Installation
* Cloner le git
* Installer les d�pendances (vendors pour le back, modules pour le fron)
	1.  A l'aide d'un terminal, placer vous dans le dossier **back** et effectuer la commande "__composer update__"
	2.  A l'aide d'un terminal, placer vous dans le dossier **front** et effectuer la commande "__npm i__"
* Cr�ation de la base de donn�e
	1.  Allez dans le dossier **back** et modifier le fichier .env pour que le **DATABASE_URL** correspondante � vos informations de connexions � votre base de donn�e MySQL
	2.  A l'aide d'un terminal, placer vous dans le dossier **back** et effectuer la commande "**php bin/console doctrine:database:create**"
	3.  A l'aide d'un terminal, placer vous dans le dossier **back** et effectuer la commande "**php bin/console doctrine:schema:update --force**"
	4.  Importez le fichier CSV **user_log** � l'aide de votre interface PhpMyAdmin

##Lancement de l'application

1. Pour lancer le back 	: lancez la commande **php -S 127.0.0.1:8000 -t public/** via un invite de commandes dans le dossier **back**
2. Pour lancer le front : lancez la commande **ng serve** via un invite de commandes dans le dossier **front**
3. Vous pouvez tester l'application avec les identifiants suivants --> email :admin@admin.com, password : Test123
